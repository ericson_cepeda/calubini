<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
    Router::connect('/', array('controller' => 'intros', 'action' => 'home'));
  Router::connect('/muri/catalogo', array('controller' => 'muri_catalogs', 'action' => 'index', 'plugin' => 'muri'));
  Router::connect('/muri/case_study', array('controller' => 'muri_case_studies', 'action' => 'index', 'plugin' => 'muri'));
  Router::connect('/muri/applicazioni', array('controller' => 'muri_applications', 'action' => 'index', 'plugin' => 'muri'));
  Router::connect('/muri/download', array('controller' => 'muri_downloads', 'action' => 'index', 'plugin' => 'muri'));
  
  Router::connect('/muri/social', array('controller' => 'muri_catalogs', 'action' => 'index', 'plugin' => 'muri'));
  Router::connect('/muri/contact', array('controller' => 'muri_catalogs', 'action' => 'index', 'plugin' => 'muri'));

	Router::connect('/admin/login', array('controller' => 'users', 'action' => 'login', 'admin' => true));
	Router::connect('/admin/index', array('controller' => 'users', 'action' => 'index', 'admin' => true));
/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
	
	
	Router::connect('/intros/get_pdf_image/:pdf',    array('controller' => 'intros', 'action' => 'get_pdf_image'),    array(  'pass' => array('pdf'),        'pdf' => '.+'    ));
