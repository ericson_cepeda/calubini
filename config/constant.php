<?php
define("CALUBINI_SELECTED_PRODUCT"					, 'Calubini.selectedProduct');
define("PIETRAVERA_URL"								, 'http://www.pietravera.com');
define("LIME_URL"						     		, 'http://www.limeonline.net');
define("SITE_URL"                                   , "http://www.calubini.com/calubini/");
define("INTRO_DEFAULT_OVERLAY_SELECTION"            , "muri");
define("INTRO_TITLE"                                , "Calubini Group 2012 - Manufatti in cemento - Muri di contenimento - Grandivasi");

/*PRODUCTS*/
define("PRODUCT_MURI"                               , "muri");
define("PRODUCT_MANUFATTI"                          , "manufatti");
define("PRODUCT_GRANDIVASI"                         , "grandivasi");
define("PRODUCT_PIETRAVERA"                         , "pietravera");

/*TITLES*/
define("TITLE_MURI"                               , "muri di contenimento");
define("TITLE_MANUFATTI"                          , "manufatti in cemento");
define("TITLE_GRANDIVASI"                         , "grandivasi");
define("TITLE_PIETRAVERA"                         , "pietravera");

/*HEADER LINKS*/

