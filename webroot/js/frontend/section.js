var $J = jQuery.noConflict();
$J(function() {
	Calubini.init();
});

var Calubini = {
	isIncompatibleBrowser: false,
	defaultParams : {
		successFunction : null,
		contentType : "json",
		data: {}
	},
	init: function(){
		Calubini.defaultParams["successFunction"] = Calubini.updateElements;
		Calubini.initSectionFrangmentLinks();
		$J("a.event-default-selection").click();
		$J("a[rel='"+$J("#selectedProduct").val()+"']").addClass("selected-product");
		$J("#selected_product").text($J('a.selected-product').text());
		$J("#section_container").on("mouseover", ".event-needs-prettyphoto", function(event) {
			Calubini.createPrettyPhoto();
		});
	},
	createPrettyPhoto : function() {
		
			$J(".event-needs-prettyphoto").each(function() {
				$J(this).removeClass("event-needs-prettyphoto").prettyPhoto({
					animation_speed : 'fast',
					slideshow : 10000,
					hideflash : true,
					allow_resize : false
				});
			})
		
	},
	initSlideLinks : function() {
		$J("li.event-slide-catalog").click(function(event){
			event.preventDefault();
			event.stopPropagation();
			var tagLink = $J(this);
			if(tagLink.is(':not(.blocked)')){
				tagLink.addClass('blocked');
				var bindList = $J('#'+tagLink.attr('bind-list'));
				if(tagLink.is(".left") && bindList.children('li:visible').last().offset().left > $J("li.event-slide-catalog.right").offset().left){
					bindList.animate({
						left: '-=149'
					},'100',function(){
						tagLink.removeClass('blocked');
					});
				}
				else if(tagLink.is(".right") && bindList.children('li:visible').first().offset().left < $J("li.event-slide-catalog.left").offset().left){
					bindList.animate({
						left: '+=149'
					},'100',function(){
						tagLink.removeClass('blocked');
					});
				}
				else
					tagLink.removeClass('blocked');				
			}

		});
	},
	initApplicationThumbnails : function() {
		$J("a.event-application-selection").click(function(event){
			event.preventDefault();
			event.stopPropagation();
			$J("#application_vga").css('background-image',"url('"+$J(this).attr('href')+"')");
			$J("#selected-application-name").text($J(this).attr('rel'));
		});
		$J("a.event-application-selection:first").click();
	},
	sendMail : function() {
		$J("a.event-send-mail").click(function(event){
			event.preventDefault();
			var params = {
				"url" : $J(this).attr('href'),
				"data" : $J("#ContactManagerProductForm").serialize(),
				'successFunction' : Calubini.emailFeedback 
			};
			
			Calubini.doRequest(params);
			
		});
	},
	emailFeedback : function(response, contentType) {
		response = $J.parseJSON(response["responseText"]);
		alert(response['response']);
	},
	initSectionFrangmentLinks : function() {/*
		$J("a.event-section-fragment").click(function(event){
			event.preventDefault();
			var params = {
				"url" : $J(this).attr('href'),
				"contentType" : "html"
			};
			
			if($J(this).is(".event-map"))
				params["successFunction"] = Calubini.initMap;
			
			Calubini.doRequest(params);
			$J(this).css("color", "#F27C00").siblings("a").removeAttr("style");
			
			$J("#section_logo").get(0).src = $J("#rootDir").val()+"img/product/"+$J("#selectedProduct").val()+"/sectionLogo/section ("+$J(this).attr('rel')+").png";
		});*/
		$J("a.event-section-fragment[rel='"+$J('#selectedSection').val()+"']").addClass('current-link-selection')
		//$J("#section_logo").get(0).src = $J("#rootDir").val()+"img/product/"+$J("#selectedProduct").val()+"/sectionLogo/section ("+$J('#selectedSection').val()+").png";;
	},
	initMap : function() {
		var myLatlng = new google.maps.LatLng(45.415232,10.392895);
	    var myOptions = {
	        zoom: 11,
	        center: myLatlng,
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    var map = new google.maps.Map(document.getElementById("contattaci_map"), myOptions);
	    
	    new google.maps.Marker({
            position: myLatlng,
            map: map,
        });
	},
	doRequest : function(params) {
		params = $J.extend({}, Calubini.defaultParams, params);
		
		url = params.url;
		
		$J.ajax({
			type : 'GET',
			url : url,
			data : params.data,
			jsonpCallback : 'jsonCallback',
			contentType : "application/" + params.contentType,
			dataType : 'jsonp',
			complete : function(response) {
				params.successFunction(response, params.contentType);
			},
			error : function(e) {

			}
		});
	},
	updateElements : function(response, contentType) {
		switch (contentType) {
		case "json":
			response = $J.parseJSON(response["responseText"]);
			for ( var key in response) {
				if (response.hasOwnProperty(key)) {
					if (Calubini.isIncompatibleBrowser == true)
						$J("#" + key).get(0).innerHtml = response[key];
					else
						$J("#" + key).html(response[key]);
					$J("#" + key).change();
				}
			}
			break;
		case "html":
			response = $J(response["responseText"]);
			response.children("*").each(
					function() {
						if (Calubini.isIncompatibleBrowser == true)
							$J("#" + $J(this).attr('id')).get(0).innerHtml = $J(
									this).get(0).innerHtml;
						else
							$J("#" + $J(this).attr('id')).html($J(this).html());
						$J("#" + $J(this).attr('id')).change();
					});
			break;
		default:
			break;
		}

		console.log("GUI updated");

	}
};
