var $J = jQuery.noConflict(true);
$J(function() {
	Calubini.init();
});

var Calubini = {
	init: function(){
		Calubini.preloadImages('img/banners/muri_s1.png', 'img/banners/manufatti_s1.png',
				'img/banners/grandivasi_s1.png', 'img/banners/pietravera_s1.png');
		Calubini.initSwapEvents();
		$J(".intro-menu span a:eq(0)").mouseenter();
	},
	initSwapEvents : function() {
		
		var introMenuWidth = $J('div.intro-menu').width();
		var introMenuLinks = $J(".intro-menu span a");
		
		var linkLength = introMenuWidth/introMenuLinks.length;
		
		introMenuLinks.each(function(){
			$J(this).width(linkLength);
		});
		
		$J("a.event-swap").on("mouseenter",function(){
			//$J('#swap').attr('src',$J(this).attr("alt"));
			Calubini.swapImage('swap','',$J(this).attr("alt"),1);
			var indexLink = $J(".intro-menu span a").index($J(this));
			var spanToShow = $J(".selection-overlay").eq(indexLink);
			
			var linkOffset = $J(this).offset();
			
			var newLeft = indexLink == introMenuLinks.length-1? linkOffset.left+5: indexLink == 0? linkOffset.left-5: linkOffset.left;
			spanToShow.css('opacity',1).offset({left:newLeft}).width(linkLength).siblings("span").css('opacity',0);
		});
	},
	preloadImages : function() {
		var d = document;
		if (d.images) {
			if (!d.MM_p)
				d.MM_p = new Array();
			var i, j = d.MM_p.length, a = Calubini.preloadImages.arguments;
			for (i = 0; i < a.length; i++)
				if (a[i].indexOf("#") != 0) {
					d.MM_p[j] = new Image;
					d.MM_p[j++].src = a[i];
				}
		}
	},
	swapImgRestore : function() {
		var i, x, a = document.MM_sr;
		for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
			x.src = x.oSrc;
	},
	findObj : function(n, d) {
		var p, i, x;
		x = $J('#' + n).get(0);
		if (!d)
			d = document;
		if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
			d = parent.frames[n.substring(p + 1)].document;
			n = n.substring(0, p);
		}
		if (!(x = d[n]) && d.all)
			x = d.all[n];
		for (i = 0; !x && i < d.forms.length; i++)
			x = d.forms[i][n];
		for (i = 0; !x && d.layers && i < d.layers.length; i++)
			x = Calubini.findObj(n, d.layers[i].document);
		if (!x && d.getElementById)
			x = d.getElementById(n);
		return x;
	},
	swapImage : function() {
		var i, j = 0, x, a = Calubini.swapImage.arguments;
		document.MM_sr = new Array;
		for (i = 0; i < (a.length - 2); i += 3)
			if ((x = Calubini.findObj(a[i])) != null) {
				document.MM_sr[j++] = x;
				if (!x.oSrc)
					x.oSrc = x.src;
				x.src = a[i + 2];
			}
	}
};
