<?php
class Intro extends AppModel {
	var $name = "Intro";
  var $actsAs = array(
      'Upload.Upload' => array(
          'photo' => array(
              'fields' => array(
                  'dir' => 'photo_dir'
              ),
              'thumbsizes' => array(
                  'xvga' => '1024x768',
                  'vga' => '640x480',
                  'thumb' => '80x80'
              ),
              'thumbnailMethod' => 'php'
          )
      )
	);
}
?>