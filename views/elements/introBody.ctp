<div class="container_12 home">
    <div class="intro-left-panel grid_8">
        <div class="home-text intro">
        	<div>
        		<?= __($text_intro['Intro']['description'],true)?>
			</div>
        </div>
    </div>
    <div class="grid_4 intro-right-panel">
    <ul class="intro-product-list">
		<?php 
			$counter = 0;
			foreach($overlays as $paragraph):
				$selectionClass = "";
				if($counter % 2 > 0)
					$selectionClass = "float-right";
		?>
		<li class="<?=$selectionClass?>">
    		<?=$this->Html->link($this->Html->image('product/'.$paragraph['Intro']['alias'].'/body_image.png', array('id'=>$paragraph['Intro']['alias'].'_body_image')).'<p>- '.__($paragraph['Intro']['name'],true).' -</p>',array('action'=>'product',$paragraph['Intro']['alias'],'home'),array('escape' => false))?>
    	</li>
         <?php $counter++; endforeach; ?>	    	
    </ul>
    </div>
</div>


