<?php
$navBarLinks = array('Homepage', 'Catalogo', 'Newsletter', 'Applicazioni', 'Download', 'Social Media', 'Contattaci')

?>

<div class="navbar">
    <!-- #BeginLibraryItem "/Library/menu.lbi" -->
    <div id="page-wrap">
        <ul class="dropdown">
        	<?php foreach($navBarLinks as $link):?>
            <li>
                <a href="index.html" title="homepage" target="_top" onmouseover="MM_swapImage('swap','','images/homepagebanner.jpg',1)"><?= __($link,true)?></a>
            </li>
			<?php endforeach;?>
        </ul>
    </div>
    <!-- #EndLibraryItem -->
</div>
