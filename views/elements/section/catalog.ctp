<div class="container_12 home">
	<?php
		foreach($completeList as $id => $downloadablePdf):
			$classCategory = "";
			if (isset($currentCategory) && $id == $currentCategory)
				$classCategory = "current-link-selection";
	?>
	<?= $this->Html->link($downloadablePdf." ~",array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'catalog',$id),array("class"=>$classCategory))?>
	<?
		endforeach;
	?>
    <table cellspacing="7">
        <tbody>
            <?php
		foreach($sectionList as $downloadablePdf):
	?>
            <tr class="catalog-item">
                <td>
                    <?php 
			$filesFolderURL = '/files/'.$filesFolderName.'/pdf/'.$downloadablePdf[$modelName]["pdf_dir"]."/".$downloadablePdf[$modelName]["pdf"]; 
			$photosFolderURL = '/files/'.$filesFolderName.'/photo/'.$downloadablePdf[$modelName]["photo_dir"]."/".$downloadablePdf[$modelName]["photo"]; 
			?>
                    <?= $this->Html->link("<span>".$this->Html->image($photosFolderURL, array('class'=>'photo','id'=>$downloadablePdf[$modelName]["name"]."_photo"))."</span>",$photosFolderURL,array('class'=>'event-needs-prettyphoto photo','escape'=>false))?>
                    
					
					<p class="name">
                        <?= $downloadablePdf[$catalogCategoryModel]['name'].'  >  '.$downloadablePdf[$modelName]["name"]?>
                    </p>
                    <iframe src="<?= SITE_URL?>intros/get_catalog_description/<?=$modelName?>/<?=$downloadablePdf[$modelName]['id']?>" class="description">
                        <?= $downloadablePdf[$modelName]["description"]?>
                    </iframe>
                    <p class="pdf">
                    	<?php
							if(file_exists(ROOT.DS.APP_DIR.DS."webroot".$filesFolderURL)):
						?>
                        <?= $this->Html->link(__('scarica scheda',true),$filesFolderURL)?>
						<?php
							else:
						?>
						<?=__('scarica scheda',true)?>
						<?php
							endif;
						?>
                    </p>
                    <p class="pdf contattaci">
                        <?= $this->Html->link(__('richieste commerciale',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'contattaci'))?>
                    </p>
                    <ul class="catalog-item-carousel">
                        <li bind-list="slide_<?= $downloadablePdf[$modelName]["id"]?>" class="slide-control event-slide-catalog left">
                        	<?=  $this->Html->image('section/catalog/catalog_rew.png',array('url'=>'#'))?>
                        </li>
                        <li class="catalog-slider">
                            <ul id="slide_<?= $downloadablePdf[$modelName]["id"]?>" class="carousel">
                                <?php
							foreach($downloadablePdf[$catalogImageModel] as $catalogImage):
								$catalogImagesFolderURL = '/files/'.$filesFolderName.'_image'.'/photo/'.$catalogImage["id"]."/thumb_".$catalogImage["photo"]; 
								$catalogFullImagesFolderURL = '/files/'.$filesFolderName.'_image'.'/photo/'.$catalogImage["id"]."/".$catalogImage["photo"]; 
								?>
                                <li>
                                    <?=  $this->Html->link($this->Html->image($catalogImagesFolderURL),$catalogFullImagesFolderURL,array('class'=>'event-needs-prettyphoto','escape'=>false))?>
                                </li>
                                <?php
							endforeach;
								?>
                            </ul>
                        </li>
                        <li bind-list="slide_<?= $downloadablePdf[$modelName]["id"]?>" class="slide-control event-slide-catalog right">
                        	<?=  $this->Html->image('section/catalog/catalog_for.png',array('url'=>'#'))?>
                        </li>
                    </ul>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<div class="catalog-paging">
    <?php if($paginator->hasPrev()):?>
    <?= $paginator->prev("<<< Indietro", array('escape'=>false), null, null);?>
    <?php endif; ?>
    <?= $paginator->numbers(array('separator'=>' | '));?>
    <?php  if($paginator->hasNext()):?>
    <?= $paginator->next("Avanti >>>", array('escape'=>false), null,null);?>
    <?php endif;?>
</div>
<?= $this->Html->css(array("prettyPhoto")) ?>
<?= $this->Html->script(array('frontend/prettyPhoto','frontend/iframe-auto-height')) ?>
<?=  $this->Html->scriptStart() ?>
$J(function(){
	Calubini.initSlideLinks();
	$J('iframe').load(function(event){
		event.stopPropagation();
		event.preventDefault();
		setTimeout(function(){
			$J(this).height($J(this).contents().find('body').height())
		}, 500);
		//$J(this).iframeAutoHeight();
	});
});
<?=  $this->Html->scriptEnd() ?>
