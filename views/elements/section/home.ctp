<div class="container_12 home">
    <div class="home-left-panel grid_8">
    	<div class="home-text section">
    		<?php /*
			$subtitles = array();
			if (preg_match("/\[([0-9],?)+\]/",$homeText[$pagesModel]['slug']))
				$subtitles = json_decode($homeText[$pagesModel]['slug']);
				
			$counter = 0;
			foreach(explode("//",$homeText[$pagesModel]['body']) as $paragraph):
				$selectionClass = "";
				if(in_array($counter, $subtitles))
					$selectionClass = "subtitle";*/
			?>
    		<div class="intro-text"><?= __($homeText['Intro']['body'],true)?></div>
    		<?php //$counter++; endforeach; ?>    		
		</div>
        <div class="home-image-container">
            <span class="home-image">
                <?= $this->Html->image('product/'.$this->Session->read(CALUBINI_SELECTED_PRODUCT).'/brochure.png', array('class'=>'brochure-image','id'=>'product_brochure','url'=>'/productFiles/'.$this->Session->read(CALUBINI_SELECTED_PRODUCT).'/catalog.pdf'))?>
				<p class="brochure-text brochure-text-event"><?=__('clicca sopra la copertina</br>per scarica il nostro brochure in pdf',true)?></p>
                <?= $this->Html->image('section/home/section_r35_c10.png', array('class'=>'brochure-text brochure-text-decoration','id'=>'section_r35_c10'))?>
            </span>
        </div>
        <?= $this->Html->image('section/home/section_r41_c7.png', array('class'=>'lemma','id'=>'section_r41_c7'))?>
    </div>
    <div class="grid_4">
    	<label class="articles-label"><?= __('novità',true)?></label>
	<ul>
        <?php
		foreach($sectionList as $downloadablePdf):
		
			$photosFolderURL = '/files/'.$filesFolderName.'/photo/'.$downloadablePdf[$modelName]["photo_dir"]."/thumb_".$downloadablePdf[$modelName]["photo"]; 
	$photosFullFolderURL = '/files/'.$filesFolderName.'/photo/'.$downloadablePdf[$modelName]["photo_dir"]."/".$downloadablePdf[$modelName]["photo"]; 
	?>
            
			<li class="home-article">
			<?= $this->Html->link($this->Html->image($photosFolderURL, array('class'=>'photo','id'=>$downloadablePdf[$modelName]["id"]."_article_photo")),$photosFullFolderURL,array('class'=>'event-needs-prettyphoto photo','escape'=>false))?>
			<p class="name">
                        <?= $downloadablePdf[$modelName]["name"]?>
                    </p>
					<div class="description">
                        <?= $downloadablePdf[$modelName]["description"]?>
                    </div>
        </li>
		<?php endforeach; ?>
		</ul>
    </div>
</div>
<?= $this->Html->css(array("prettyPhoto")) ?>
<?= $this->Html->script(array('frontend/prettyPhoto')) ?>