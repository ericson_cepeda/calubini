<div class="container_12 download">
	<?php
		foreach($completeList as $id => $downloadablePdf):
			$classCategory = "";
			if (isset($currentCategory) && $id == $currentCategory)
				$classCategory = "current-link-selection";
	?>
	<?= $this->Html->link($downloadablePdf." ~",array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'download',$id),array("class"=>$classCategory))?>
	<?
		endforeach;
	?>
	<ul class="download-section">
	<?php
		foreach($sectionList as $downloadablePdf):
	?>
		<li>
			<?php $filesFolderURL = '/files/'.$filesFolderName.'/file/'.$downloadablePdf[$modelName]["file_dir"]."/".$downloadablePdf[$modelName]["file"]; ?>
			<?= $this->Html->image(SITE_URL.'intros/get_pdf_image/pdf:'.$filesFolderURL, array('class'=>'brochure-image','id'=>'product_brochure','url'=>$filesFolderURL))?>
			<p><?= $downloadablePdf[$modelName]["name"]?></p>
		</li>
	<?php endforeach;?>
	</ul>
</div>
