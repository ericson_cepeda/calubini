<div class="section-menu">
	<span id="selected_product"></span>
    <span class="options">
        <?= $html->link(__('Homepage',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'home'),array('rel'=>'home','class'=>'event-section-fragment event-default-selection')) ?>
        <?= $html->link(__('Catalogo',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'catalog'),array('rel'=>'catalog','class'=>'catalogo-link event-section-fragment')) ?>
        <?= $html->link(__('Newsletter',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'case'),array('rel'=>'case','class'=>'case-link event-section-fragment')) ?>
        <?= $html->link(__('Galleria',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'application'),array('rel'=>'application','class'=>'event-section-fragment')) ?>
        <?= $html->link(__('Download',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'download'),array('rel'=>'download','class'=>'event-section-fragment')) ?>
        <?= $html->link(__('Social media',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'social'),array('rel'=>'social','class'=>'social-link event-section-fragment')) ?>
		<?= $html->link(__('Contattaci',true),array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'contattaci'),array('rel'=>'contattaci','class'=>'contattaci-link event-map event-section-fragment')) ?>
    </span>
</div>
