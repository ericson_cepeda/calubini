<div class="container_12 home">
    <div class="home-left-panel grid_8">
        <div class="application-vga centered">
            <div id="application_vga">
            </div>
            <?= $this->Html->image('section/home/section_r35_c10.png', array('class'=>'application-text-decoration','id'=>'section_r35_c10'))?>
			<p id="selected-application-name" class="application-text"></p>
        </div>
    </div>
    <div class="grid_4">
        <ul id="slide_application" class="carousel application">
            <?php
							foreach($sectionList as $applicationImage):
								$applicationImagesFolderURL = '/files/'.$filesFolderName.'/img/'.$applicationImage[$modelName]["id"]."/thumb_".$applicationImage[$modelName]["img"]; 
								$applicationFullImagesFolderURL = '/files/'.$filesFolderName.'/img/'.$applicationImage[$modelName]["id"]."/".$applicationImage[$modelName]["img"]; 
								?>
            <li>
                <?=  $this->Html->link($this->Html->image($applicationImagesFolderURL),$applicationFullImagesFolderURL,array('rel'=>$applicationImage[$modelName]["name"],'class'=>'event-application-selection','escape'=>false))?>
            </li>
            <?php
							endforeach;
								?>
        </ul>
    </div>
</div>
<?=  $this->Html->scriptStart() ?>
$J(function(){
Calubini.initApplicationThumbnails();
});
<?=  $this->Html->scriptEnd() ?>
