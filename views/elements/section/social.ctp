<div class="container_12 home">
    <p class="social-info"><?=__('Interagisci con noi sulle più importanti piattaforme social.',true)?></p>
	<ul class="social-accounts">
		<li class="social-account"><p class="centered brief"><?=__('Scopri la nostra Pagina Facebook, dove potrai dialogare con noi e ricevere informazioni utili',true)?></p><?=$this->Html->link(__('Venite a trovarci sulla nostra pagina',true),$links['Intro']['facebook'])?></li>
		<li class="social-account"><p class="centered brief tube"><?=__('Vedi in anteprima i nostri video sul nostro canale YouTube, commentali con gli amici e condividili su web',true)?></p><?=$this->Html->link(__('Venite a trovarci sul nostro canale',true),$links['Intro']['youtube'])?></li>
		<li class="social-account"><p class="centered brief tweet"><?=__('Il servizio è attivo anche sul nostro profilo Twitter, pronto a rispondervi',true)?></p><?=$this->Html->link(__('Seguici su Twitter',true),$links['Intro']['twitter'])?></li>
	</ul>
</div>
