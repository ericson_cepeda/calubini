<div class="container_12 home">
    <div class="home-left-panel grid_8">
        <div class="contattaci-map centered">
            <div id="contattaci_map">
            </div>
        </div>
		<div class="contattaci-data">
			<?= $this->Html->image('section/contattaci/contattaci_r3_c2_s1.png', array('class'=>'contattaci-phone-logo','id'=>'contattaci_r3_c2_s1'))?>
			<?= $this->Html->image('section/contattaci/contattaci_r4_c4_s1.png', array('class'=>'contattaci-phone','id'=>'contattaci_r4_c4_s1'))?>
		</div>
    </div>
    <div class="grid_4 contattaci-form-container">
        <?= $this->Form->create('ContactManager.Contact');?>
        <ul class="contattaci-info">
            <li>
                <?= $this->Form->input('Message.nome', array('label' => __('Nome e Cognome',true)));?>
            </li>
            <li>
                <?= $this->Form->input('Message.azienda', array('label' => __('Azienda',true)));?>
            </li>
            <li>
                <div>
                    <label>
                        <?=__('Indirizzo',true)?>
                    </label>
                    <?= $this->Form->textarea('Message.indirizzo', array('label' => __('Indirizzo',true)));?>
                </div>
            </li>
            <li>
                <?= $this->Form->input('Message.telefono', array('label' => __('Telefono',true)));?>
            </li>
            <li>
                <?= $this->Form->input('Message.email', array('label' => __('Email',true)));?>
            </li>
            <li>
                <div>
                    <label>
                        <?=__('Richiesta',true)?>
                    </label>
                    <?= $this->Form->textarea('Message.richiesta', array('label' => __('Richiesta',true)));?>
                </div>
            </li>
        </ul>
		<?= $this->Form->end(); ?>
        <ul class="contattaci-info">
            <li>
                <div>
                    <label>
                        <?=__('Privacy 196',true)?>
                    </label>
                    <?= $this->Form->textarea('privacy', array('label' => __('Privacy 196',true),'readonly'=>'readonly', 'value'=>__("CALUBINI S.r.l. informa che:\n\n1. I dati personali sono raccolti al fine di provvedere agli adempimenti connessi a: - Realizzazione dell'annuario degli indirizzi CALUBINI S.r.l. - Comunicazione di eventi formativi, eventi sociali ed opportunita di lavoro - Realizzazione del database CALUBINI S.r.l. I dati saranno trattati in modo lecito, secondo correttezza e con la massima riservatezza; saranno registrati, organizzati, e conservati in archivi informatici e/o cartacei.\n\n2.Nell'ambito dei trattamenti descritti la raccolta dei dati quali cognome, nome e recapito, assume natura obbligatoria per l'espletamento degli adempimenti di cui sopra.\n\n3.I dati personali raccolti, nei limiti dell'espletamento delle finalita di cui sopra, possono essere comunicati a: Societa di head hunting collegate da rapporti di partnership con l'assocazione CALUBINI S.r.l. Trasferiti per le medesime finalita ad altre associazioni Alumni di master certificati collegate alla associazione CALUBINI S.r.l. A con sede in Italia ed all'estero.\n\n4.L'eventuale rifiuto di comunicare un'informazione comportera l'impossibilita dello scrivente a garantire il corretto trattamento dei suoi dati in ambito dei nostri rapporti di partnership.\n\n5.Si informa che all'interessato competono i diritti ai sensi dell'art. 13 Legge 675/96 ed in particolare l'ottenimento della conferma circa l'esistenza dei dati che lo riguardano conoscerne l'origine, la logica e la finalita su cui si basa il trattamento; ottenere la cancellazione, trasformazione o il blocco dei dati trattati illegittimamente, nonch� l'aggiornamento la rettifica e l'integrazione qualora ne abbia interesse; opporsi, per motivi legittimi, al trattamento dei dati stessi.\n\nCon l'occasione porgiamo distinti saluti.\n\nCALUBINI S.r.l. -25018 Montichiari (BS) Italia - T .+39 030 961793 - M. info@calubini.com - NR. ISCR. CAM. COMMERCIO 02820870984 - CAP.SOCIALE � 100.000,00 I.V. - REA 481397\n\nsi Acconsento al trattamento.",true)));?>
                </div>
            </li>
        </ul>		
        
        <?= $html->link(__('invia',true),array('action'=>'send_email'),array('class'=>'float-right event-send-mail'))?>
    </div>
</div>
<?=  $this->Html->scriptStart() ?>
$J(function(){
	Calubini.initMap();
	Calubini.sendMail();
});
<?=  $this->Html->scriptEnd() ?>