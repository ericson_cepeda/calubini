<div class="container_12 home">
    <?php
		foreach($completeList as $id => $downloadablePdf):
			$classCategory = "";
			if (isset($currentCase) && $id == $currentCase)
				$classCategory = "current-link-selection";
	?>
    <?= $this->Html->link($downloadablePdf." ~",array('action'=>'product',$this->Session->read(CALUBINI_SELECTED_PRODUCT),'case',$id),array("class"=>$classCategory))?>
    <?
		endforeach;
	?>
    <table cellspacing="7">
        <tbody>
            <?php
		foreach($sectionList as $downloadablePdf):
	?>
            <tr class="catalog-item case">
                <td>
                    <?php 
			$photosFolderURL = '/files/'.$filesFolderName.'/photo/'.$downloadablePdf[$modelName]["photo_dir"]."/".$downloadablePdf[$modelName]["photo"]; 
			?>
                    <?= $this->Html->link($this->Html->image($photosFolderURL, array('class'=>'photo','id'=>$downloadablePdf[$modelName]["name"]."_photo")),$photosFolderURL,array('class'=>'event-needs-prettyphoto photo','escape'=>false))?>
                    <p class="name">
                        <?= $downloadablePdf[$modelName]["name"]?>
                    </p>
                    <iframe src="<?= SITE_URL?>intros/get_catalog_description/<?=$modelName?>/<?=$downloadablePdf[$modelName]['id']?>" class="description case">
                        <?= $downloadablePdf[$modelName]["description"]?>
                    </iframe>
                    <ul class="float-left case-links">
                        <li>
                        	<?= $this->Html->image('section/case/case_r13_c4.jpg', array('id'=>'case_r13_c4','alt' => ''))?>
                            <label><?= $this->Html->link(__("Galleria fotografica",true),$downloadablePdf[$modelName]["album_link"]) ?></label>
                        </li>
                        <li>
                            <?= $this->Html->image('section/case/case_r17_c3.jpg', array('id'=>'case_r17_c3','alt' => ''))?>
							<label><?= $this->Html->link(__("Youtube video",true),$downloadablePdf[$modelName]["youtube_link"]) ?></label>
                        </li>
                    </ul>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>
<?= $this->Html->css(array("prettyPhoto")) ?>
<?= $this->Html->script(array('frontend/prettyPhoto')) ?>
<?=  $this->Html->scriptStart() ?>
$J(function(){
})
<?=  $this->Html->scriptEnd() ?>