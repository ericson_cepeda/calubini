<div class="intro-menu">
	<span>
		<?php 
			$counter = 0;
			foreach($overlays as $paragraph):
				$selectionClass = "";
				if($paragraph['Intro']['alias'] == INTRO_DEFAULT_OVERLAY_SELECTION)
					$selectionClass = " event-default-selection";
				if($counter > 0)
					$selectionClass .= " intro-menu-link";
		?>
		<?= $html->link($html->image('product/'.$paragraph['Intro']['alias'].'/link_intro.png',array('class'=>'intro-menu-link-image')),array('action'=>'product',$paragraph['Intro']['alias'],'home'),array('alt'=>'img/banners/'.$paragraph['Intro']['alias'].'_s1.jpg','class'=>$paragraph['Intro']['alias'].' event-swap'.$selectionClass, 'escape'=>false)) ?>
         <?php $counter++; endforeach; ?>	
    </span>
</div>