<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Login - Admin</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes"> 
    <?= $this->Html->css(array("base-admin","pages/signin","bootstrap.min","/bootstrap-responsive.min","font-awesome","http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600")) ?>
	<?= $html->script(array('jquery-1.7.2.min','bootstrap','signin'));?>
</head>
<body>
	
	<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="./">
				Base Admin				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="/" class="">
							<i class="icon-chevron-left"></i>
							Back to Homepage
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    <div class="account-container register">
        <div class="content clearfix">
        	<?php echo $this->Form->create('User'); ?>
                <h1>Sign In</h1>
                <div class="login-fields">

                    <p>Sign in using your registered account:</p>

                    <div class="field">
                        <label for="username">Username:</label>
                        <input type="text" id="username" name="data[User][username]" value="" placeholder="username" class="login"/>
                    </div> <!-- /field -->

                    <div class="field">
                        <label for="password">Password:</label>
                        <input type="password" id="passwd" name="data[User][passwd]" value="" placeholder="passwd" class="login"/>
                    </div> <!-- /field -->

                </div> <!-- /login-fields -->

                <div class="login-actions">
				<input type="submit" value="Login" class="button btn btn-primary btn-large">
			<?php echo $this->Form->end(); ?>

                </div> <!-- .actions -->
            </form>
        </div> <!-- /content -->
    </div> <!-- /account-container -->
</body>
</html>