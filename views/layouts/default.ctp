<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html>
    <head>
    	<?php
			$titles = array(PRODUCT_MURI => TITLE_MURI, PRODUCT_MANUFATTI => TITLE_MANUFATTI,PRODUCT_GRANDIVASI=>TITLE_GRANDIVASI,PRODUCT_PIETRAVERA=>TITLE_PIETRAVERA);
		?>
        <title><?=__($titles[$this->Session->read(CALUBINI_SELECTED_PRODUCT)],true)?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<base href="<?=SITE_URL?>">
        <?= $this->Html->css(array("frontend/960","frontend/style","http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600")) ?>
        <?= $this->Html->script(array('jquery-1.8.2.min','frontend/section')) ?>
		<?= $html->script('http://maps.googleapis.com/maps/api/js?sensor=false');?>
        <!--Fireworks CS5 Dreamweaver CS5 target.  Created Sat Nov 03 15:30:06 GMT-0500 (SA Pacific Standard Time) 2012-->
        <script type="text/javascript">
            
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-301102-64']);
            _gaq.push(['_trackPageview']);
            
            (function(){
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
            
        </script>		
    </head>
    <body>
        <span class="header-overlay">
            <?= $this->Html->image('section/section_r17_c1_s1.png', array('class'=>'header-blackbar bar','id'=>'section_r17_c1_s1','alt' => ''))?>
            <?= $this->Html->image('section/section_r17_c23_s1.png', array('class'=>'header-orangebar bar','id'=>'section_r17_c23_s1','alt' => ''))?>
        </span>
        <div class="main-container centered">
            <div class="header">
				<?= $this->Form->hidden('rootDir',array('value'=>SITE_URL))?>
				<?= $this->Form->hidden('selectedSection',array('value'=>$selectedSection))?>
            	<?= $this->Form->hidden('selectedProduct',array('value'=>$this->Session->read(CALUBINI_SELECTED_PRODUCT)))?>
                <?= $this->Html->image('section/section_r3_c7.png', array('class'=>'header-logo','id'=>'section_r3_c7','alt' => ''))?>
                <span class="sections">
                    <ul class="section-product-list">
					<?php 
						$counter = 0;
						foreach($overlays as $paragraph):
					?>
						<li>
							<?=$this->Html->link(__($paragraph['Intro']['name'],true),array('action'=>'product',$paragraph['Intro']['alias'],'home'),array('escape' => false,'rel'=>$paragraph['Intro']['alias']))?>
				    	</li>
			         <?php $counter++; endforeach; ?>	                      	
                    </ul>
                </span>
            </div>
            <?= $this->Html->image('section/section_r18_c1.png', array('class'=>'left-shadow shadow','id'=>'section_r18_c1','alt' => ''))?>
            <?= $this->Html->image('section/section_r19_c21.png', array('class'=>'right-shadow shadow','id'=>'section_r19_c21','alt' => ''))?>
            <div class="main-overlay centered">
                <div class="main-content centered">
                    <?= $content_for_layout ?>
                </div>
            </div>
        </div>
    </body>
</html>
