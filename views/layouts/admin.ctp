<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dashboard - Base Admin</title>
    <base href="<?=SITE_URL?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">    
    <?= $this->Html->css(array("pages/dashboard","base-admin","base-admin-responsive","pages/signin","bootstrap.min","/bootstrap-responsive.min","font-awesome","http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600")) ?>
	<?= $html->script(array('jquery-1.7.2.min','excanvas.min','excanvas.min','jquery.flot','jquery.flot.pie','jquery.flot.orderBars','jquery.flot.resize','bootstrap','base','charts/area','charts/donut'));?>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php echo $scripts_for_layout; ?>
        <?= $html->scriptStart() ?>
			$(function(){
	            tinyMCE.init({
	                theme : "advanced",
	                mode : "textareas",
	                theme_advanced_toolbar_location : "top",
	            });
			});
        <?= $html->scriptEnd() ?>
  </head>

<body>

<div class="navbar navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<a class="brand" href="./">
				Calubini				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					<li class="dropdown">
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user"></i> 
							<?php echo $this->Session->read('Auth.usernmame'); ?>
							<b class="caret"></b>
						</a>
						
						<ul class="dropdown-menu">
							<li><?=$this->Html->link(__('Logout',true),"/admin/users/logout",array('escape' => false,'rel'=>'admin'))?></li>
						</ul>
						
					</li>
				</ul>
			
			</div><!--/.nav-collapse -->
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->
    



    
<div class="subnavbar">

	<div class="subnavbar-inner">
	
		<div class="container">

			<ul class="mainnav">

				<li class="dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-share-alt"></i>
						<span>Intro</span>
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
						<li><?=$this->Html->link(__('Box',true),SITE_URL.'admin/intros/')?></li>
						<li><?=$this->Html->link(__('Testo Intro',true),SITE_URL.'admin/intros/description')?></li>
					</ul>  				
				</li>
			
				<li class="dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-share-alt"></i>
						<span>Muri</span>
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
						<li><?=$this->Html->link(__('Newsletter Categorie',true),SITE_URL.'admin/muri/muri_category_case_studies')?></li>
						<li><?=$this->Html->link(__('Newsletter',true),SITE_URL.'admin/muri/muri_case_studies')?></li>
						<li class="divider"></li>
						<li><?=$this->Html->link(__('Catalogo Categorie',true),SITE_URL.'admin/muri/muri_category_catalogs')?></li>
						<li><?=$this->Html->link(__('Catalogo',true),SITE_URL.'admin/muri/muri_catalogs')?></li>
                        <li><?=$this->Html->link(__('Immagini Catalogo',true),SITE_URL.'admin/muri/muri_catalog_images')?></li>
						<li class="divider"></li>
						<li><?=$this->Html->link(__('Download Categorie',true),SITE_URL.'admin/muri/muri_category_downloads')?></li>
						<li><?=$this->Html->link(__('Download',true),SITE_URL.'admin/muri/muri_downloads')?></li>
						<li><?=$this->Html->link(__('News',true),SITE_URL.'admin/muri/muri_articles')?></li>
						<!--<li class="divider"></li>
						<li><?=$this->Html->link(__('Pages',true),SITE_URL.'admin/muri/muri_pages')?></li>-->
						<li class="divider"></li>
						<li><?=$this->Html->link(__('Galleria',true),SITE_URL.'admin/muri/muri_applications')?></li>
					</ul>    				
				</li>

				<li class="dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-share-alt"></i>
						<span>Manufatti</span>
						<b class="caret"></b>
					</a>

                    <ul class="dropdown-menu">
                        <li><?=$this->Html->link(__('Newsletter Categorie',true),SITE_URL.'admin/manufatti/manufatti_category_case_studies')?></li>
                        <li><?=$this->Html->link(__('Newsletter',true),SITE_URL.'admin/manufatti/manufatti_case_studies')?></li>
                        <li class="divider"></li>
                        <li><?=$this->Html->link(__('Catalogo Categorie',true),SITE_URL.'admin/manufatti/manufatti_category_catalogs')?></li>
                        <li><?=$this->Html->link(__('Catalogo',true),SITE_URL.'admin/manufatti/manufatti_catalogs')?></li>
                        <li><?=$this->Html->link(__('Immagini Catalogo',true),SITE_URL.'admin/manufatti/manufatti_catalog_images')?></li>
                        <li class="divider"></li>
                        <li><?=$this->Html->link(__('Download Categorie',true),SITE_URL.'admin/manufatti/manufatti_category_downloads')?></li>
                        <li><?=$this->Html->link(__('Download',true),SITE_URL.'admin/manufatti/manufatti_downloads')?></li>
						<li><?=$this->Html->link(__('News',true),SITE_URL.'admin/muri/manufatti_articles')?></li>
                        <!--<li class="divider"></li>
                        <li><?=$this->Html->link(__('Pages',true),SITE_URL.'admin/manufatti/manufatti_pages')?></li>-->
                        <li class="divider"></li>
                        <li><?=$this->Html->link(__('Galleria',true),SITE_URL.'admin/manufatti/manufatti_applications')?></li>
                    </ul>
                </li>

				<li class="dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-share-alt"></i>
						<span>Grandi Vasi</span>
						<b class="caret"></b>
					</a>

                    <ul class="dropdown-menu">
                        <li><?=$this->Html->link(__('Newsletter Categorie',true),SITE_URL.'admin/grandivasi/grandivasi_category_case_studies')?></li>
                        <li><?=$this->Html->link(__('Newsletter',true),SITE_URL.'admin/grandivasi/grandivasi_case_studies')?></li>
                        <li class="divider"></li>
                        <li><?=$this->Html->link(__('Catalogo Categorie',true),SITE_URL.'admin/grandivasi/grandivasi_category_catalogs')?></li>
                        <li><?=$this->Html->link(__('Catalogo',true),SITE_URL.'admin/grandivasi/grandivasi_catalogs')?></li>
                        <li><?=$this->Html->link(__('Immagini Catalogo',true),SITE_URL.'admin/grandivasi/grandivasi_catalog_images')?></li>
                        <li class="divider"></li>
                        <li><?=$this->Html->link(__('Download Categorie',true),SITE_URL.'admin/grandivasi/grandivasi_category_downloads')?></li>
                        <li><?=$this->Html->link(__('Download',true),SITE_URL.'admin/grandivasi/grandivasi_downloads')?></li>
						<li><?=$this->Html->link(__('News',true),SITE_URL.'admin/muri/grandivasi_articles')?></li>
                        <!--<li class="divider"></li>
                        <li><?=$this->Html->link(__('Pages',true),SITE_URL.'admin/grandivasi/grandivasi_pages')?></li>-->
                        <li class="divider"></li>
                        <li><?=$this->Html->link(__('Galleria',true),SITE_URL.'admin/grandivasi/grandivasi_applications')?></li>
                    </ul>
				</li>
				<!--
				<li class="dropdown">					
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-share-alt"></i>
						<span>More Pages</span>
						<b class="caret"></b>
					</a>	
				
					<ul class="dropdown-menu">
						<li><?=$this->Html->link(__('Charts',true),'./charts.html')?></li>
						<li><?=$this->Html->link(__('User Account',true),'./account.html')?></li>
						<li class="divider"></li>
						<li><?=$this->Html->link(__('Login',true),'./login.html')?></li>
						<li><?=$this->Html->link(__('Signup',true),'./signup.html')?></li>
						<li><?=$this->Html->link(__('Error',true),'./error.html')?></li>
					</ul>    				
				</li>
				-->
			</ul>

		</div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div> <!-- /subnavbar -->
    
    
<div class="main">
	
	<div class="main-inner">

	    <div class="container">
	    	<?php echo $content_for_layout ?>
	    </div> <!-- /container -->
	    
	</div> <!-- /main-inner -->
    
</div> <!-- /main -->

<div class="footer">
	
	<div class="footer-inner">
		
		<div class="container">
			
			<div class="row">
				
    			<div class="span12">
    				&copy; 2012 <?=$this->Html->link(__('Limeonline',true),'http://www.limeonline.net/')?>.
    			</div> <!-- /span12 -->
    			
    		</div> <!-- /row -->
    		
		</div> <!-- /container -->
		
	</div> <!-- /footer-inner -->
	
</div> <!-- /footer -->

</body>
</html>
