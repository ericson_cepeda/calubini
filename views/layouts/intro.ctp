<!DOCTYPE html>
<!-- saved from url=(0014)about:internet -->
<html>
    <head>
        <title><?=__(INTRO_TITLE,true)?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<base href="<?=SITE_URL?>">
        <?= $this->Html->css(array("frontend/960","frontend/style")) ?>
		<?= $this->Html->script(array('jquery-1.8.2.min','frontend/intro')) ?>
        <!--Fireworks CS5 Dreamweaver CS5 target.  Created Sat Nov 03 15:30:06 GMT-0500 (SA Pacific Standard Time) 2012-->
        <script type="text/javascript">
            
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-301102-64']);
            _gaq.push(['_trackPageview']);
            
            (function(){
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
            
        </script>
    </head>
    <body>
        <span class="header-overlay">
            <?= $this->Html->image('section/section_r17_c1_s1.png', array('class'=>'header-blackbar bar','id'=>'section_r17_c1_s1','alt' => ''))?>
            <?= $this->Html->image('section/section_r17_c23_s1.png', array('class'=>'header-orangebar bar','id'=>'section_r17_c23_s1','alt' => ''))?>
        </span>
        <div class="main-container centered">
            <div class="header intro">
                <?= $this->Html->image('intro/intro_r1_c10.png', array('class'=>'header-logo-intro','id'=>'intro_r1_c10','alt' => ''))?>
            </div>
            <?= $this->Html->image('section/section_r18_c1.png', array('class'=>'left-shadow shadow','id'=>'section_r18_c1','alt' => ''))?>
            <?= $this->Html->image('section/section_r19_c21.png', array('class'=>'right-shadow shadow','id'=>'section_r19_c21','alt' => ''))?>
            <div class="main-overlay centered">
                <div class="main-content centered">
					<?= $content_for_layout ?>
                </div>
            </div>
        </div>
    </body>
</html>
