<!-- File: /app/views/posts/index.ctp -->

<h1>Boxes</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/intros/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Box', '/admin/intros/add'); ?>
<table class="table table-striped table-bordered">
	<tr>
		<th>Id</th>
		<th>Name</th>
		<th>Description</th>
        <th>Url</th>
		<th>Order</th>
		<th>Created</th>
        <th>Action</th>
	</tr>

	<!-- Here is where we loop through our $posts array, printing out post info -->

	<?php foreach ($boxs as $box): 
			$hideLinkText = "Hide";
			if ($box['Intro']['hidden'] == 1)
				$hideLinkText = "Show";
	?>
	<tr>
		<td><?php echo $box['Intro']['id']; ?></td>
		<td>
			<?php echo $box['Intro']['name']; ?>
		</td>
		<td>
			<?php echo $box['Intro']['description']; ?>
		</td>
        <td>
            <?php echo $box['Intro']['url']; ?>
        </td>
		<td><?php echo $box['Intro']['order']; ?></td>
		<td><?php echo $box['Intro']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'intros','action'=>'edit',$box['Intro']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'intros','action'=>'delete',$box['Intro']['id'], 'admin' => true)))." | ".$this->Html->link($hideLinkText,array('controller'=>'intros','action'=>'hide',$box['Intro']['id'], 'admin' => true)); ?>
        </td>
	</tr>
	<?php endforeach; ?>

</table>