<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Box</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('Intro', array('action' => 'admin_description'));
                echo $form->input('Intro.id', array('type' => 'hidden'));
                echo $form->input('Intro.name', array('label' => 'Name'));
				echo $this->TinyMce->input('Intro.description');
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>