<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Box</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('Intro', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('Intro.id', array('type' => 'hidden'));
                echo $form->input('Intro.name', array('label' => 'Name'));
                echo $this->Form->input('Intro.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('Social.photo_dir', array('type' => 'hidden'));
                echo $form->input('Intro.description', array('label' => 'Description'));
				echo $this->TinyMce->input('Intro.body');
                echo $form->input('Intro.url', array('label' => 'Url href'));
                echo $form->input('Intro.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>