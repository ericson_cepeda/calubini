<div class="section-menu-container">
    <?= $this->element('introMenu')?>
    <div class="intro-logo">
        <label>
            <?= $this->Html->image('banners/intro1_s1.png', array('id'=>'swap', 'alt'=>''))?>
        </label>
		<?php foreach($overlays as $paragraph):
				//$paragraph = $overlays[$i];
		?>
        <span class="selection-overlay <?= $paragraph['Intro']['alias']?>">
			<label><?= $this->Html->image('banners/'.$paragraph['Intro']['alias'].'.png', array('id'=>$paragraph['Intro']['alias'].'_photo', 'alt'=>''))?></label>
			<p class="centered"><?=__($paragraph['Intro']['description'],true)?></p>
			<?= $this->Html->image('banners/menuInfo_r9_c7_s1.png', array('class'=>'link', 'alt'=>'','url'=>array('action'=>'product',$paragraph['Intro']['alias'],'home')))?>
		</span>
         <?php endforeach; ?>
    </div>
</div>
<div>
	<?= $this->element('introBody')?>
</div>
<div>
    <p class="footer-text-intro">
    	<span>
    	<?=__('CALUBINI S.r.l. - 25018 Montichiari (BS) Italia - T .+39 030 961793 - M. info@calubini.com',true)?>
		</span>
		<?=__('NR. ISCR. CAM. COMMERCIO 02820870984 - CAP.SOCIALE € 100.000,00 I.V. - REA 481397',true)?>
    </p>
	<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50826d4f3e819e15"></script>
<!-- AddThis Button END -->
<div class="footer-links-intro">
	<?= $html->link(__('homepage',true)." ~ ",array('action'=>'home'),array('alt'=>'img/banners/'.$paragraph['Intro']['alias'].'_s1.png','class'=>'', 'escape'=>false)) ?>
			<?php 
			$counter = 0;
			foreach($overlays as $paragraph):
				$separator = "";
				if ($counter < sizeof($overlays)-1)
					$separator = " ~ ";
		?>
		<?= $html->link($paragraph['Intro']['name'].$separator,array('action'=>'product',$paragraph['Intro']['alias'],'home'),array('alt'=>'img/banners/'.$paragraph['Intro']['alias'].'_s1.png','class'=>'', 'escape'=>false)) ?>
         <?php $counter++; endforeach; ?>
		 <?= $html->link(__('webcredits',true),LIME_URL,array('alt'=>'img/banners/'.$paragraph['Intro']['alias'].'_s1.png','class'=>'float-right', 'escape'=>false)) ?>
		 </div>	
</div>