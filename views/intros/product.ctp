<div class="section-menu-container">
    <?= $this->element('section/menu')?>
    <div class="section-logos">
        <label>
        	<?php 
			$selectedSectionImage = $selectedSection;
			if($this->Session->read(CALUBINI_SELECTED_PRODUCT) != PRODUCT_MURI)
				$selectedSectionImage = "home";
			?>
            <?= $this->Html->image('product/'.$this->Session->read(CALUBINI_SELECTED_PRODUCT).'/sectionLogo/section ('.$selectedSectionImage.').jpg', array('id' => 'section_logo'))?>
        </label>
        <label>
            <?= $this->Html->image('section/orange/'.$this->Session->read(CALUBINI_SELECTED_PRODUCT).'.png', array('class' => 'float-right'))?>
        </label>
    </div>
</div>
<div id="section_container">
	<?= $this->element('section/'.$selectedSection)?>
</div>
<div>
    <p class="footer-text">
    	<?=__('CALUBINI S.r.l. - 25018 Montichiari (BS) Italia - T .+39 030 961793 - M. info@calubini.com  - NR. ISCR. CAM. COMMERCIO 02820870984 - CAP.SOCIALE € 100.000,00 I.V. - REA 481397',true)?>
    </p>
</div>
