<?php
class AppController extends Controller {
	var $name = "App";
	var $components = array('Auth', 'Session', 'Email');
	var $helpers = array('Session', 'Form', 'Html', 'TinyMce.TinyMce');

	function beforeFilter(){
		$this->Auth->allow('*');
		$this->Auth->autoRedirect = false;
		if(isset($this->params['prefix'])){
			$this->layout = "admin";
		}
	}
}
?>