<?php
class IntrosController extends AppController {
	var $name = "Intros";
	var $components = array('Email');
	function admin_index(){
		if($this->Session->read('logged') == true){
			$boxs = $this->Intro->find('all', array('conditions' => array("slug" != "description")));
			$this->set('boxs', $boxs);
		}
		else {
			$this->redirect('/admin/users/login');
		}
	}

	function admin_description(){
		if($this->Session->read('logged') == true){
			$this->layout = "admin";
			if(empty($this->data)){
				$this->data = $this->Intro->find('first', array('conditions' => array('slug' => "description")));
			}
			else {
				$this->data['Intro']['slug'] = "description";
				if($this->Intro->save($this->data)){
					$this->Session->setFlash('Descrizione modificata con successo.');
					$this->redirect('/admin/intros/index');
				}
			}
		}
		else {
			$this->redirect('/admin/users/login');
		}
	}

	function admin_add(){
		if($this->Session->read('logged') == true){
			$this->layout = "admin";
			if(!empty($this->data)){
				if($this->Intro->save($this->data)){
					$this->Session->setFlash('Lavoro aggiunto con successo.');
					$this->redirect('/admin/intros/index');
				}
				else {
					$this->Session->setFlash('Errore durante il salvataggio.');
					$this->redirect('/admin/intros/index');
				}
			}
		}
		else {
			$this->redirect('/admin/users/login');
		}
	}

	function admin_edit($id = null){
		if($this->Session->read('logged') == true){
			$this->layout = "admin";
			$this->Intro->id = $id;
			if(empty($this->data)){
				$this->data = $this->Intro->read();
			}
			else {
				if($this->Intro->save($this->data)){
					$this->Session->setFlash('Lavoro modificata con successo.');
					$this->redirect('/admin/intros/index');
				}
			}
 		}
 		else {
 			$this->redirect('/admin/users/login');
 		}
	}

	function admin_delete($id){
 		if($this->Session->read('logged') == true){
			$this->layout = "admin";
			$this->Intro->delete($id);
			$this->Session->setFlash('Cancellato con successo');
			$this->redirect('/admin/intros/index');
		}
		else {
			$this->redirect('/admin/users/login');
		}
	}
	
	function admin_hide($id){
		$this->Intro->id = $id;
 		$hiddenAttribute = $this->Intro->read('hidden');
		$newStatus = $hiddenAttribute["Intro"]["hidden"] == false ? true : false;
		$this->Intro->saveField('hidden', $newStatus );
		$this->Session->setFlash('Successo');
		$this->redirect('/admin/intros/index');
	}

	function home(){
		$this->set('text_intro', $this->Intro->find('first', array('conditions' => array('slug' => 'description'))));
		$this->set('overlays', $this->Intro->find('all', array('conditions' => array('Intro.slug' => "overlay", "hidden"=>0), 'order' => array('Intro.order ASC'))));
		$this->layout = "intro";
	}

	function product($selectedProduct = null, $selectedSection = null, $extraParameter = null){
		
		if ($selectedProduct == null || $selectedSection == null)
			$this->redirect("/");
		else if ($selectedProduct == 'pietravera')
			$this->redirect(PIETRAVERA_URL);
		$this->Session->write(CALUBINI_SELECTED_PRODUCT,$selectedProduct);
		$this->set("selectedSection", $selectedSection);
		$this->set('overlays', $this->Intro->find('all', array('conditions' => array("hidden"=>0), 'order' => array('Intro.order ASC'))));
		
		$conditions = array();
		$this->paginate = array();
		$pageLimit = 3;
		$modelProduct = null;
		$modelsToBind = null;
		$needsList = false;
		$filesFolderName = $selectedProduct."_".$selectedSection;
		switch ($selectedSection) {
			case "download":
				$modelProduct = ucfirst($selectedSection);
				$downloadCategoryModel = ucfirst($selectedProduct)."Category".$modelProduct;
				
				if ($extraParameter){
					$this->set("currentCategory",$extraParameter);
				}
				
				$this->loadModel($downloadCategoryModel);
				$this->set("completeList",$this->{$downloadCategoryModel}->find('list'));
				
				$this->set("downloadCategoryModel",$downloadCategoryModel);
				$pageLimit = 10;
				break;
			case "catalog":
				$modelProduct = ucfirst($selectedSection);
				$modelCatalog = ucfirst($selectedProduct).$modelProduct;
				$catalogCategoryModel = ucfirst($selectedProduct)."Category".ucfirst($selectedSection);
				$catalogImageModel = $modelCatalog."Image";
				$this->loadModel($catalogCategoryModel);
				if (!$extraParameter){
					$firstCatalog = $this->{$catalogCategoryModel}->find('first');
					$extraParameter = $firstCatalog[$catalogCategoryModel]['id'];
				}
				
					$this->set("currentCategory",$extraParameter);
					$conditions = array('conditions'=>array($selectedProduct.'_category_catalog_id'=>$extraParameter));
					$this->paginate[$modelCatalog] = $conditions;

				$categoryBindArray = array('belongsTo'=>array($catalogCategoryModel=>array('foreignKey'=>$selectedProduct.'_category_catalog_id')));

				$modelsToBind = array(array('hasMany'=>array($catalogImageModel=>array('foreignKey'=>$selectedProduct.'_catalog_id'))),$categoryBindArray);

				
				$this->set("completeList",$this->{$catalogCategoryModel}->find('list'));
				
				$this->set("catalogCategoryModel",$catalogCategoryModel);
				$this->set("catalogImageModel",$catalogImageModel);
				break;
			case "application":
				$modelProduct = ucfirst($selectedSection);
				$filesFolderName = $selectedProduct."_".$selectedSection;
				$pageLimit = 1000;
				break;
			case "home":
				$modelProduct = "Article";
				$filesFolderName = $selectedProduct."_article";
				
				$pagesModel = ucfirst($selectedProduct).'Page';
				$this->loadModel('Intro');
				$this->set("pagesModel",$pagesModel);
				$this->set("homeText",$this->Intro->find('first',array('conditions'=>array('alias'=>$selectedProduct))));
				break;
			case "case":
				$modelProduct = "CaseStudy";
				$modelCase = ucfirst($selectedProduct).$modelProduct;
				$caseCategoryModel = ucfirst($selectedProduct)."Category".$modelProduct;
				$this->loadModel($modelCase);
				if (!$extraParameter){
					$firstCase = $this->{$modelCase}->find('first');
					$extraParameter = $firstCase[$modelCase]['id'];
				}
				
					$this->set("currentCase",$extraParameter);
					$conditions = array('conditions'=>array($modelCase.'.id'=>$extraParameter));
					$this->paginate[$modelCase] = $conditions;
				
				$categoryBindArray = array('belongsTo'=>array($caseCategoryModel=>array('foreignKey'=>$selectedProduct.'_category_case_study_id')));
				$modelsToBind = array(array($categoryBindArray));
				
				$filesFolderName = $selectedProduct."_".$selectedSection."_study";
				
				
				$this->set("completeList",$this->{$modelCase}->find('list'));
				$this->set("caseCategoryModel",$caseCategoryModel);
				break;
			case "social":
				$this->set('links', $this->Intro->find('first', array('conditions' => array('Intro.slug' => "overlay", "Intro.alias"=>$selectedProduct), 'fields' => array('Intro.facebook','Intro.twitter','Intro.youtube'))));
				break;
				
			default:

				break;
		}

		if($modelProduct){
			$modelName = ucfirst($selectedProduct).$modelProduct;
			$this->loadModel($modelName);
				
			$this->paginate[$modelName]['order'] = array($modelName.'.order' => 'asc');
			$this->paginate[$modelName]['limit'] = $pageLimit;
			if($modelsToBind){
				foreach ($modelsToBind as $arrayBind){
					$this->{$modelName}->bindModel($arrayBind, false);
				}
			}
			$this->set("filesFolderName",$filesFolderName);
			$this->set("modelName",$modelName);
			
			$this->set("sectionList",$this->paginate($modelName));
			
			
		}

		//print each file name


	}

	function get_pdf_image($pdf = null){
		$this->layout = 'ajax';
		$pdf = explode(":",$pdf);
		$pdf = $pdf[1];
		try
		{
			$pdf = ROOT.DS.APP_DIR.DS."webroot".$pdf;//SITE_URL.$pdf;
			
			$width = 126;
			//$tmp = ROOT.DS.APP_DIR.DS."webroot".'tmp';
			$format = "png";
			$source = $pdf.'[0]';
			$dest = "$pdf.$format";
			preg_match("/(.+\/).+\.pdf/", $pdf,$matches);
			//debug($matches, $showHTML = false, $showFrom = true);die;
			@chmod($matches[1], 0777);/*$image->writeImage($saveToDir . $imageName);
			if (!file_exists($dest))
			{
				$exec = "convert -scale $width $source $dest";
				exec($exec);
			}*/
			if (!file_exists($dest))
			{
				$handle = fopen($pdf, 'rb');
				$im = new Imagick();
				$im->readImageFile($handle);
				$im->resizeImage(126,178, imagick::FILTER_LANCZOS, 0.9);
				$im->setImageFormat( $format );
				$im->writeImage($dest);
			}
			else{
				$im = new Imagick($dest);
				$im->setImageFormat( $format );				
			}
			header("Content-Type:".$im->getFormat());
			echo $im;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	function get_catalog_description($modelName = null, $catalogId = null){
		$this->layout = "ajax";
		$ModelInstance = ClassRegistry::init($modelName);
		$catalogFound = $ModelInstance->find('first',array("fields"=>array("description"),"conditions"=>array($modelName.".id = ".$catalogId)));
		$this->set("description",$catalogFound[$modelName]["description"]);
	}

	function section_fragment($selectedSection = null){
		//path to directory to scan
		$directory = ROOT.DS.APP_DIR.DS."webroot/files".DS.$selectedProduct."_".$selectedSection;

		if (is_dir($directory)) {
			//get all files in specified directory
			$Directory = new RecursiveDirectoryIterator($directory);
			$Iterator = new RecursiveIteratorIterator($Directory);
			$Regex = new RegexIterator($Iterator, '/^.+\.pdf$/i', RecursiveRegexIterator::GET_MATCH);
				
				
			$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory),
					RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($Regex as $path) {
				var_dump($path);
			}
		}


		$files = glob($directory . DS ."*.{pdf}", GLOB_BRACE);
	}
	
	function send_email() {
		$this->layout = "ajax";
		$this->autorender = false;
		if (isset($this->params['url']['data']) && !empty($this->params['url']['data']['Message'])) {
			$result = array();
			
			$this->loadModel('Message');
			$messageArray = array();
			$messageArray['Message'] = $this->params['url']['data']['Message'];
				
			$date = getdate();
			$messageArray["Message"]['date_time'] = date(DATE_ATOM);
			$this->Message->create();
			if ($this->Message->save($messageArray)) {
				try {
					$this->Session->setFlash(__('message.sent', true));
					
					$this->Email->to = 'info@calubini.com';
					$this->Email->subject = 'Contact message';
					$this->Email->from = $messageArray["Message"]['email'];
					$ccArray = array('t.diaconu@calubini.com');
					$this->Email->cc = $ccArray;
					$this->Email->sendAs = 'both';
// 					$this->Email->delivery = 'debug';
					$this->set('message',$messageArray["Message"]);
					
					$emailString = "";
					foreach ($messageArray["Message"] as $key => $property) {
						$emailString.="\n".$key.': '.$property;
					}
					
					$this->Email->send($emailString);
					
					$result['response'] = __('Messaggio inviato.', true);
					$result['error'] = $this->Email->smtpError;
					//echo $this->flash('email');
					
				} catch (Exception $e) {
					$result['response'] = __('Messaggio non inviato.', true);
					$result['error'] = __($e->getMessage(), true);
				}
				
				
				
				//$this->render('sent');
				//$result = $this->render('sent');
				
				//$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('message.not_sent', true));
				$result['error'] = __('message.not_sent', true);
			}
			echo json_encode($result);
		}
	}
}