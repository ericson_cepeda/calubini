<?php
class UsersController extends AppController {
	var $name = "Users";

	function beforeRender(){
		//debug($this->Session->read('Auth'));
	}

	function signup(){
		if(!empty($this->data)){
			$this->User->create();
			if($this->User->save($this->data)){
				$this->redirect('/');
			}
		}
	}

	function login(){
		
	}

	function admin_logout(){
		$this->Session->write('logged',false);
		$this->Session->destroy();
		$this->redirect('/admin/users/login');
	}

	function admin_index(){
		if($this->Session->read('logged') == true){
			$this->layout = "admin";
		}
		else {
			$this->redirect('/admin/users/login');
		}
	}

	function admin_login(){
		
		if($this->Session->read('logged') == true){
			$this->redirect('/admin/users/index');
		}
		else {
			if(!empty($this->data)){
				$userFound = $this->User->find('first',array('conditions'=>array('username'=>$this->data['User']['username'],'password'=>sha1($this->data['User']['passwd']))));
				if ($userFound)
					$this->Session->write('logged',true);
				else{
					$this->Session->write('logged',false);
					$this->redirect('/admin/users/login');
				}
			}
			else {
				$this->layout="admin_login";
			}
		}
	}
}
?>