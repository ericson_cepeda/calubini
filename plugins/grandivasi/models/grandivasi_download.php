<?php
class GrandivasiDownload extends GrandivasiAppModel {
	var $name = 'GrandivasiDownload';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'GrandivasiCategoryDownload' => array(
			'className' => 'GrandivasiCategoryDownload',
			'foreignKey' => 'grandivasi_category_download_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    var $actsAs = array(
        'Upload.Upload' => array(
            'file' => array(
                'fields' => array(
                    'dir' => 'file_dir'
                )
            )
        )
    );
}
