<?php
class GrandivasiCategoryCatalog extends GrandivasiAppModel {
	var $name = 'GrandivasiCategoryCatalog';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	var $hasMany = array(
		'GrandivasiCatalog' => array(
			'className' => 'GrandivasiCatalog',
			'foreignKey' => 'grandivasi_category_catalog_id'
		)
	);
}
