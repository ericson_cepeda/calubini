<?php
class GrandivasiAppController extends AppController {

	function beforeFilter(){
		if($this->Session->read('logged') != true){
			$this->redirect('/admin/users/login');
		}
		$this->Auth->allow('*');
		$this->Auth->autoRedirect = false;
		if($this->params['prefix']){
			$this->layout = "admin";
		}
	}
}
?>