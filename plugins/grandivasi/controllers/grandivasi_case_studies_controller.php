<?php
class GrandivasiCaseStudiesController extends GrandivasiAppController {

	var $name = 'GrandivasiCaseStudies';

	function index() {
		$this->GrandivasiCaseStudy->recursive = 0;
		$this->set('grandivasiCaseStudies', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCaseStudy', $this->GrandivasiCaseStudy->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiCaseStudy->create();
			if ($this->GrandivasiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi case study could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCaseStudy->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Grandivasi case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiCaseStudies', $this->GrandivasiCaseStudy->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCaseStudy', $this->GrandivasiCaseStudy->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiCaseStudy->create();
			if ($this->GrandivasiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi case study could not be saved. Please, try again.', true));
			}
		}
        $this->set('categories', $this->GrandivasiCaseStudy->GrandivasiCategoryCaseStudy->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCaseStudy->read(null, $id);
            $this->set('categories', $this->GrandivasiCaseStudy->GrandivasiCategoryCaseStudy->find('list', array('fields' => array('id', 'name'))));
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Grandivasi case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
