<?php
class GrandivasiCategoryDownloadsController extends GrandivasiAppController {

	var $name = 'GrandivasiCategoryDownloads';

	function index() {
		$this->GrandivasiCategoryDownload->recursive = 0;
		$this->set('grandivasiCategoryDownloads', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi category download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCategoryDownload', $this->GrandivasiCategoryDownload->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiCategoryDownload->create();
			if ($this->GrandivasiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category download could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi category download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCategoryDownload->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi category download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCategoryDownload->delete($id)) {
			$this->Session->setFlash(__('Grandivasi category download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi category download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiCategoryDownloads', $this->GrandivasiCategoryDownload->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi category download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCategoryDownload', $this->GrandivasiCategoryDownload->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiCategoryDownload->create();
			if ($this->GrandivasiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category download could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi category download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCategoryDownload->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi category download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCategoryDownload->delete($id)) {
			$this->Session->setFlash(__('Grandivasi category download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi category download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
