<?php
class GrandivasiCatalogsController extends GrandivasiAppController {

	var $name = 'GrandivasiCatalogs';

	function index() {
		$this->GrandivasiCatalog->recursive = 0;
		$this->set('grandivasiCatalogs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCatalog', $this->GrandivasiCatalog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiCatalog->create();
			if ($this->GrandivasiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCatalog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCatalog->delete($id)) {
			$this->Session->setFlash(__('Grandivasi catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiCatalogs', $this->GrandivasiCatalog->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCatalog', $this->GrandivasiCatalog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiCatalog->create();
			if ($this->GrandivasiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi catalog could not be saved. Please, try again.', true));
			}
		}
        $this->set('categories', $this->GrandivasiCatalog->GrandivasiCategoryCatalog->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCatalog->read(null, $id);
            $this->set('categories', $this->GrandivasiCatalog->GrandivasiCategoryCatalog->find('list', array('fields' => array('id', 'name'))));
        }
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCatalog->delete($id)) {
			$this->Session->setFlash(__('Grandivasi catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
