<?php
class GrandivasiApplicationsController extends GrandivasiAppController {

	var $name = 'GrandivasiApplications';

	function index() {
		$this->GrandivasiApplication->recursive = 0;
		$this->set('grandivasiApplications', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi application', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiApplication', $this->GrandivasiApplication->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiApplication->create();
			if ($this->GrandivasiApplication->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi application could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi application', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiApplication->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi application could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiApplication->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi application', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiApplication->delete($id)) {
			$this->Session->setFlash(__('Grandivasi application deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi application was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiApplications', $this->GrandivasiApplication->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi application', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiApplication', $this->GrandivasiApplication->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiApplication->create();
			if ($this->GrandivasiApplication->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi application could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi application', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiApplication->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi application could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiApplication->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi application', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiApplication->delete($id)) {
			$this->Session->setFlash(__('Grandivasi application deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi application was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
