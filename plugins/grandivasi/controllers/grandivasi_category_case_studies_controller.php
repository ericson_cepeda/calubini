<?php
class GrandivasiCategoryCaseStudiesController extends GrandivasiAppController {

	var $name = 'GrandivasiCategoryCaseStudies';

	function index() {
		$this->GrandivasiCategoryCaseStudy->recursive = 0;
		$this->set('grandivasiCategoryCaseStudies', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCategoryCaseStudy', $this->GrandivasiCategoryCaseStudy->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiCategoryCaseStudy->create();
			if ($this->GrandivasiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category case study could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCategoryCaseStudy->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi category case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCategoryCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Grandivasi category case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi category case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->GrandivasiCategoryCaseStudy->recursive = 0;
		$this->set('grandivasiCategoryCaseStudies', $this->GrandivasiCategoryCaseStudy->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCategoryCaseStudy', $this->GrandivasiCategoryCaseStudy->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiCategoryCaseStudy->create();
			if ($this->GrandivasiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category case study could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCategoryCaseStudy->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi category case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCategoryCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Grandivasi category case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi category case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
