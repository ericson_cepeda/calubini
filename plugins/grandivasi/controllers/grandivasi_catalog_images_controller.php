<?php
class GrandivasiCatalogImagesController extends GrandivasiAppController {

    var $name = 'GrandivasiCatalogImages';

    function admin_index() {
        $this->GrandivasiCatalog->recursive = 0;
        $this->set('grandivasiCatalogImages', $this->GrandivasiCatalogImage->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid grandivasi catalog', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('grandivasiCatalogImages', $this->GrandivasiCatalogImage->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->GrandivasiCatalogImage->create();
            if ($this->GrandivasiCatalogImage->save($this->data)) {
                $this->Session->setFlash(__('The grandivasi catalog has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The grandivasi catalog could not be saved. Please, try again.', true));
            }
        }
        $this->set('categories', $this->GrandivasiCatalogImage->GrandivasiCatalog->find('list', array('fields' => array('id', 'name'))));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid grandivasi catalog', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->GrandivasiCatalogImage->save($this->data)) {
                $this->Session->setFlash(__('The grandivasi catalog has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The grandivasi catalog could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->GrandivasiCatalogImage->read(null, $id);
            $this->set('categories', $this->GrandivasiCatalogImage->GrandivasiCatalog->find('list', array('fields' => array('id', 'name'))));
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for grandivasi catalog', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->GrandivasiCatalogImage->delete($id)) {
            $this->Session->setFlash(__('Grandivasi catalog deleted', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Grandivasi catalog was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
}
