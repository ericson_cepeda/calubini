<?php
class GrandivasiCategoryCatalogsController extends GrandivasiAppController {

	var $name = 'GrandivasiCategoryCatalogs';

	function index() {
		$this->GrandivasiCategoryCatalog->recursive = 0;
		$this->set('grandivasiCategoryCatalogs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCategoryCatalog', $this->GrandivasiCategoryCatalog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiCategoryCatalog->create();
			if ($this->GrandivasiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCategoryCatalog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi category catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCategoryCatalog->delete($id)) {
			$this->Session->setFlash(__('Grandivasi category catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi category catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiCategoryCatalogs', $this->GrandivasiCategoryCatalog->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiCategoryCatalog', $this->GrandivasiCategoryCatalog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiCategoryCatalog->create();
			if ($this->GrandivasiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi category catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiCategoryCatalog->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi category catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiCategoryCatalog->delete($id)) {
			$this->Session->setFlash(__('Grandivasi category catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi category catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
