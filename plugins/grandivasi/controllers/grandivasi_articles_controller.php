<?php
class GrandivasiArticlesController extends GrandivasiAppController {

	var $name = 'GrandivasiArticles';

	function index() {
		$this->GrandivasiArticle->recursive = 0;
		$this->set('grandivasiArticles', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi article', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiArticle', $this->GrandivasiArticle->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiArticle->create();
			if ($this->GrandivasiArticle->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi article could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi article', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiArticle->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi article could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiArticle->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiArticle->delete($id)) {
			$this->Session->setFlash(__('Grandivasi article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi article was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiArticles', $this->GrandivasiArticle->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi article', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiArticle', $this->GrandivasiArticle->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiArticle->create();
			if ($this->GrandivasiArticle->find('count') < 3)
				if ($this->GrandivasiArticle->save($this->data)) {
					$this->Session->setFlash(__('The grandivasi article has been saved', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The grandivasi article could not be saved. Please, try again.', true));
				}
			else {
				$this->Session->setFlash(__('Maximum three articles are allowed.', true));
				$this->redirect(array('action' => 'index'));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi article', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiArticle->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi article could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiArticle->read(null, $id);
        }
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiArticle->delete($id)) {
			$this->Session->setFlash(__('Grandivasi article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi article was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
