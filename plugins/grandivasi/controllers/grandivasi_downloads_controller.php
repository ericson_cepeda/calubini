<?php
class GrandivasiDownloadsController extends GrandivasiAppController {

	var $name = 'GrandivasiDownloads';

	function index() {
		$this->GrandivasiDownload->recursive = 0;
		$this->set('grandivasiDownloads', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiDownload', $this->GrandivasiDownload->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->GrandivasiDownload->create();
			if ($this->GrandivasiDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi download could not be saved. Please, try again.', true));
			}
		}
		$grandivasiCategoryDownloads = $this->GrandivasiDownload->GrandivasiCategoryDownload->find('list');
		$this->set(compact('grandivasiCategoryDownloads'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiDownload->read(null, $id);
		}
		$grandivasiCategoryDownloads = $this->GrandivasiDownload->GrandivasiCategoryDownload->find('list');
		$this->set(compact('grandivasiCategoryDownloads'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiDownload->delete($id)) {
			$this->Session->setFlash(__('Grandivasi download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('grandivasiDownloads', $this->GrandivasiDownload->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid grandivasi download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('grandivasiDownload', $this->GrandivasiDownload->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->GrandivasiDownload->create();
			if ($this->GrandivasiDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi download could not be saved. Please, try again.', true));
			}
		}
    $this->set('categories', $this->GrandivasiDownload->GrandivasiCategoryDownload->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid grandivasi download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->GrandivasiDownload->save($this->data)) {
				$this->Session->setFlash(__('The grandivasi download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grandivasi download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->GrandivasiDownload->read(null, $id);
		}
    $this->set('categories', $this->GrandivasiDownload->GrandivasiCategoryDownload->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for grandivasi download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->GrandivasiDownload->delete($id)) {
			$this->Session->setFlash(__('Grandivasi download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Grandivasi download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
