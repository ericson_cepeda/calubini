<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Newsletter Categorie</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_category_case_studies/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Newsletter', '/admin/grandivasi/grandivasi_category_case_studies/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiCategoryCaseStudies as $grandivasiCategoryCaseStudie): ?>
    <tr>
        <td><?php echo $grandivasiCategoryCaseStudie['GrandivasiCategoryCaseStudy']['id']; ?></td>
        <td>
            <?php echo $grandivasiCategoryCaseStudie['GrandivasiCategoryCaseStudy']['name']; ?>
        </td>
        <td><?php echo $grandivasiCategoryCaseStudie['GrandivasiCategoryCaseStudy']['order']; ?></td>
        <td><?php echo $grandivasiCategoryCaseStudie['GrandivasiCategoryCaseStudy']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_category_case_studies','action'=>'edit',$grandivasiCategoryCaseStudie['GrandivasiCategoryCaseStudy']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_category_case_studies','action'=>'delete',$grandivasiCategoryCaseStudie['GrandivasiCategoryCaseStudy']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>