<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Grandivasi Category Case Study</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiCategoryCaseStudy', array('action' => 'admin_add'));
                echo $form->input('GrandivasiCategoryCaseStudy.name', array('label' => 'Name'));
                echo $form->input('GrandivasiCategoryCaseStudy.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>