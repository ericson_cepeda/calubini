<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Category Catalog</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_category_catalogs/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Catalog', '/admin/grandivasi/grandivasi_category_catalogs/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiCategoryCatalogs as $grandivasiCategoryCatalog): ?>
    <tr>
        <td><?php echo $grandivasiCategoryCatalog['GrandivasiCategoryCatalog']['id']; ?></td>
        <td>
            <?php echo $grandivasiCategoryCatalog['GrandivasiCategoryCatalog']['name']; ?>
        </td>
        <td><?php echo $grandivasiCategoryCatalog['GrandivasiCategoryCatalog']['order']; ?></td>
        <td><?php echo $grandivasiCategoryCatalog['GrandivasiCategoryCatalog']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_category_catalogs','action'=>'edit',$grandivasiCategoryCatalog['GrandivasiCategoryCatalog']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_category_catalogs','action'=>'delete',$grandivasiCategoryCatalog['GrandivasiCategoryCatalog']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>