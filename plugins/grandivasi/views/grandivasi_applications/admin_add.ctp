<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Application</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiApplication', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('GrandivasiApplication.name', array('label' => 'Name'));
                echo $this->Form->input('GrandivasiApplication.img', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('GrandivasiApplication.img_dir', array('type' => 'hidden'));
                echo $form->input('GrandivasiApplication.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>