<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Applications</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_applications/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Applications', '/admin/grandivasi/grandivasi_applications/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiApplications as $grandivasiApplication): ?>
    <tr>
        <td><?php echo $grandivasiApplication['GrandivasiApplication']['id']; ?></td>
        <td>
            <?php echo $grandivasiApplication['GrandivasiApplication']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/grandivasi_application/img/<?php echo $grandivasiApplication['GrandivasiApplication']['img_dir']; ?>/thumb_<?php echo $grandivasiApplication['GrandivasiApplication']['img'] ?>"></td>
        <td><?php echo $grandivasiApplication['GrandivasiApplication']['order']; ?></td>
        <td><?php echo $grandivasiApplication['GrandivasiApplication']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_applications','action'=>'edit',$grandivasiApplication['GrandivasiApplication']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_applications','action'=>'delete',$grandivasiApplication['GrandivasiApplication']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>