<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Image Gallery</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_catalog_images/add', array('escape' => false)); ?><?php echo $html->link('Grandivasi Newsletter Images', '/admin/grandivasi/grandivasi_catalog_images/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiCatalogImages as $grandivasiCatalogImage): ?>
    <tr>
        <td><?php echo $grandivasiCatalogImage['GrandivasiCatalogImage']['id']; ?></td>
        <td>
            <?php echo $grandivasiCatalogImage['GrandivasiCatalogImage']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/grandivasi_catalog_image/photo/<?php echo $grandivasiCatalogImage['GrandivasiCatalogImage']['id']; ?>/thumb_<?php echo $grandivasiCatalogImage['GrandivasiCatalogImage']['photo'] ?>"></td>
        <td><?php echo $grandivasiCatalogImage['GrandivasiCatalogImage']['order']; ?></td>
        <td><?php echo $grandivasiCatalogImage['GrandivasiCatalogImage']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_catalog_images','action'=>'edit',$grandivasiCatalogImage['GrandivasiCatalogImage']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_catalog_images','action'=>'delete',$grandivasiCatalogImage['GrandivasiCatalogImage']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>