<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Grandivasi Catalog Image</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiCatalogImage', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('GrandivasiCatalogImage.name', array('label' => 'Name'));
                echo $this->Form->input('GrandivasiCatalogImage.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('GrandivasiCatalogImage.photo_dir', array('type' => 'hidden'));
                echo $form->input('GrandivasiCatalogImage.order', array('label' => 'Order'));
                echo $form->input('GrandivasiCatalogImage.grandivasi_catalog_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>