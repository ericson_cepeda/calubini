<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Download</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiDownload', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('GrandivasiDownload.id', array('type' => 'hidden'));
                echo $form->input('GrandivasiDownload.name', array('label' => 'Name'));
                echo $this->TinyMce->input('GrandivasiDownload.description');
                echo $this->Form->input('GrandivasiDownload.file', array('type' => 'file', 'label' => 'Pdf'));
                echo $this->Form->input('GrandivasiDownload.file_dir', array('type' => 'hidden'));
                echo $form->input('GrandivasiDownload.order', array('label' => 'Order'));
                echo $form->input('GrandivasiDownload.grandivasi_category_download_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>