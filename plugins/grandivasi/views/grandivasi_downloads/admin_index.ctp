<!-- File: /app/views/posts/index.ctp -->

<h1>Downloads</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_downloads/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Download', '/admin/grandivasi/grandivasi_downloads/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiDownloads as $grandivasiDownload): ?>
    <tr>
        <td><?php echo $grandivasiDownload['GrandivasiDownload']['id']; ?></td>
        <td>
            <?php echo $grandivasiDownload['GrandivasiDownload']['name']; ?>
        </td>
		<td><?= $html->link(__('Preview',true),$this->webroot."/files/grandivasi_download/file/".$grandivasiDownload['GrandivasiDownload']['file_dir'].'/'.$grandivasiDownload['GrandivasiDownload']['file'],array()) ?></td>
        <td><?php echo $grandivasiDownload['GrandivasiDownload']['order']; ?></td>
        <td><?php echo $grandivasiDownload['GrandivasiDownload']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_downloads','action'=>'edit',$grandivasiDownload['GrandivasiDownload']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_downloads','action'=>'delete',$grandivasiDownload['GrandivasiDownload']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>