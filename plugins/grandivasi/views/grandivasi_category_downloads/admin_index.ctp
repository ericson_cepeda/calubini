<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Category Downloads</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_category_downloads/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Catalog', '/admin/grandivasi/grandivasi_category_downloads/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiCategoryDownloads as $grandivasiCategoryDownload): ?>
    <tr>
        <td><?php echo $grandivasiCategoryDownload['GrandivasiCategoryDownload']['id']; ?></td>
        <td>
            <?php echo $grandivasiCategoryDownload['GrandivasiCategoryDownload']['name']; ?>
        </td>
        <td><?php echo $grandivasiCategoryDownload['GrandivasiCategoryDownload']['order']; ?></td>
        <td><?php echo $grandivasiCategoryDownload['GrandivasiCategoryDownload']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_category_downloads','action'=>'edit',$grandivasiCategoryDownload['GrandivasiCategoryDownload']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_category_downloads','action'=>'delete',$grandivasiCategoryDownload['GrandivasiCategoryDownload']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>