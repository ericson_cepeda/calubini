<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Newsletter</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_case_studies/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Newsletter', '/admin/grandivasi/grandivasi_case_studies/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiCaseStudies as $grandivasiCaseStudie): ?>
    <tr>
        <td><?php echo $grandivasiCaseStudie['GrandivasiCaseStudy']['id']; ?></td>
        <td>
            <?php echo $grandivasiCaseStudie['GrandivasiCaseStudy']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/grandivasi_case_study/photo/<?php echo $grandivasiCaseStudie['GrandivasiCaseStudy']['photo_dir']; ?>/thumb_<?php echo $grandivasiCaseStudie['GrandivasiCaseStudy']['photo'] ?>"</td>
        <td><?php echo $grandivasiCaseStudie['GrandivasiCaseStudy']['order']; ?></td>
        <td><?php echo $grandivasiCaseStudie['GrandivasiCaseStudy']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_case_studies','action'=>'edit', $grandivasiCaseStudie['GrandivasiCaseStudy']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_case_studies','action'=>'delete', $grandivasiCaseStudie['GrandivasiCaseStudy']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>