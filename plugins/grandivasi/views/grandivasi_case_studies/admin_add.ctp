<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Grandivasi Case Study</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiCaseStudy', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('GrandivasiCaseStudy.name', array('label' => 'Name'));
				echo $this->TinyMce->input('GrandivasiCaseStudy.description');
                echo $this->Form->input('GrandivasiCaseStudy.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('GrandivasiCaseStudy.photo_dir', array('type' => 'hidden'));
                echo $form->input('GrandivasiCaseStudy.album_link', array('label' => 'Album Link'));
                echo $form->input('GrandivasiCaseStudy.youtube_link', array('label' => 'YouTube Link'));
                echo $form->input('GrandivasiCaseStudy.order', array('label' => 'Order'));
                echo $form->input('GrandivasiCaseStudy.muri_category_case_study_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>