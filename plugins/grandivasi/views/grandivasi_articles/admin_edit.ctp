<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Grandivasi Article</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiArticle', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('GrandivasiArticle.id', array('type' => 'hidden'));
                echo $form->input('GrandivasiArticle.name', array('label' => 'Name'));
                echo $this->TinyMce->input('GrandivasiArticle.description');
                echo $this->Form->input('GrandivasiArticle.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('GrandivasiArticle.photo_dir', array('type' => 'hidden'));
                echo $form->input('GrandivasiArticle.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>