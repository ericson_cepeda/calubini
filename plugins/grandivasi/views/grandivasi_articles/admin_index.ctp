<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Articles</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_articles/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Articolo', '/admin/grandivasi/grandivasi_articles/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiArticles as $grandivasiArticle): ?>
    <tr>
        <td><?php echo $grandivasiArticle['GrandivasiArticle']['id']; ?></td>
        <td>
            <?php echo $grandivasiArticle['GrandivasiArticle']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/grandivasi_article/photo/<?php echo $grandivasiArticle['GrandivasiArticle']['photo_dir']; ?>/thumb_<?php echo $grandivasiArticle['GrandivasiArticle']['photo'] ?>"></td>
        <td><?php echo $grandivasiArticle['GrandivasiArticle']['order']; ?></td>
        <td><?php echo $grandivasiArticle['GrandivasiArticle']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_articles','action'=>'edit',$grandivasiArticle['GrandivasiArticle']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_articles','action'=>'delete',$grandivasiArticle['GrandivasiArticle']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>