<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Grandivasi Catalog</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('GrandivasiCatalog', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('GrandivasiCatalog.name', array('label' => 'Name'));
                echo $this->TinyMce->input('GrandivasiCatalog.description');
                echo $this->Form->input('GrandivasiCatalog.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('GrandivasiCatalog.photo_dir', array('type' => 'hidden'));
                echo $this->Form->input('GrandivasiCatalog.pdf', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('GrandivasiCatalog.pdf_dir', array('type' => 'hidden'));
                echo $form->input('GrandivasiCatalog.order', array('label' => 'Order'));
                echo $form->input('GrandivasiCatalog.grandivasi_category_catalog_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>