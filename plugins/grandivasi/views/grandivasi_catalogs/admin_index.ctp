<!-- File: /app/views/posts/index.ctp -->

<h1>Grandivasi Catalogs</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/grandivasi/grandivasi_catalogs/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Prodotto', '/admin/grandivasi/grandivasi_catalogs/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($grandivasiCatalogs as $grandivasiCatalog): ?>
    <tr>
        <td><?php echo $grandivasiCatalog['GrandivasiCatalog']['id']; ?></td>
        <td>
            <?php echo $grandivasiCatalog['GrandivasiCatalog']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/grandivasi_catalog/photo/<?php echo $grandivasiCatalog['GrandivasiCatalog']['photo_dir']; ?>/thumb_<?php echo $grandivasiCatalog['GrandivasiCatalog']['photo'] ?>"></td>
        <td><?php echo $grandivasiCatalog['GrandivasiCatalog']['order']; ?></td>
        <td><?php echo $grandivasiCatalog['GrandivasiCatalog']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'grandivasi_catalogs','action'=>'edit',$grandivasiCatalog['GrandivasiCatalog']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'grandivasi_catalogs','action'=>'delete',$grandivasiCatalog['GrandivasiCatalog']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>