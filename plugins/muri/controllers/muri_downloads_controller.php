<?php
class MuriDownloadsController extends MuriAppController {

	var $name = 'MuriDownloads';

	function index() {
		$this->MuriDownload->recursive = 0;
		$this->set('muriDownloads', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriDownload', $this->MuriDownload->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MuriDownload->create();
			if ($this->MuriDownload->save($this->data)) {
				$this->Session->setFlash(__('The muri download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri download could not be saved. Please, try again.', true));
			}
		}
		$muriCategoryDownloads = $this->MuriDownload->MuriCategoryDownload->find('list');
		$this->set(compact('muriCategoryDownloads'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriDownload->save($this->data)) {
				$this->Session->setFlash(__('The muri download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriDownload->read(null, $id);
		}
		$muriCategoryDownloads = $this->MuriDownload->MuriCategoryDownload->find('list');
		$this->set(compact('muriCategoryDownloads'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriDownload->delete($id)) {
			$this->Session->setFlash(__('Muri download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->MuriDownload->recursive = 0;
		$this->set('muriDownloads', $this->MuriDownload->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriDownload', $this->MuriDownload->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriDownload->create();
			if ($this->MuriDownload->save($this->data)) {
				$this->Session->setFlash(__('The muri download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri download could not be saved. Please, try again.', true));
			}
		}
    $this->set('categories', $this->MuriDownload->MuriCategoryDownload->find('list', array('fields' => array('id', 'name'))));

	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriDownload->save($this->data)) {
				$this->Session->setFlash(__('The muri download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriDownload->read(null, $id);
		}
    $this->set('categories', $this->MuriDownload->MuriCategoryDownload->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriDownload->delete($id)) {
			$this->Session->setFlash(__('Muri download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
