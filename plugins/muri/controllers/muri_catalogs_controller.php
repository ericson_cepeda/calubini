<?php
class MuriCatalogsController extends MuriAppController {

	var $name = 'MuriCatalogs';

	function index() {
		$this->MuriCatalog->recursive = 0;
		$this->set('muriCatalogs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCatalog', $this->MuriCatalog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MuriCatalog->create();
			if ($this->MuriCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCatalog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCatalog->delete($id)) {
			$this->Session->setFlash(__('Muri catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->MuriCatalog->recursive = 0;
		$this->set('muriCatalogs', $this->MuriCatalog->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCatalog', $this->MuriCatalog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriCatalog->create();
			if ($this->MuriCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri catalog could not be saved. Please, try again.', true));
			}
		}
        $this->set('categories', $this->MuriCatalog->MuriCategoryCatalog->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCatalog->read(null, $id);
            $this->set('categories', $this->MuriCatalog->MuriCategoryCatalog->find('list', array('fields' => array('id', 'name'))));
        }
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCatalog->delete($id)) {
			$this->Session->setFlash(__('Muri catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
