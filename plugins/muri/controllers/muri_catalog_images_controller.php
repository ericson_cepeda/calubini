<?php
class MuriCatalogImagesController extends MuriAppController {

    var $name = 'MuriCatalogImages';

    function admin_index() {
        $this->set('muriCatalogImages', $this->MuriCatalogImage->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid muri catalog', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('muriCatalogImages', $this->MuriCatalog->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->MuriCatalogImage->create();
            if ($this->MuriCatalogImage->save($this->data)) {
                $this->Session->setFlash(__('The muri catalog has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The muri catalog could not be saved. Please, try again.', true));
            }
        }
        $this->set('categories', $this->MuriCatalogImage->MuriCatalog->find('list', array('fields' => array('id', 'name'))));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid muri catalog', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->MuriCatalogImage->save($this->data)) {
                $this->Session->setFlash(__('The muri catalog has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The muri catalog could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->MuriCatalogImage->read(null, $id);
            $this->set('categories', $this->MuriCatalogImage->MuriCatalog->find('list', array('fields' => array('id', 'name'))));
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for muri catalog', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->MuriCatalogImage->delete($id)) {
            $this->Session->setFlash(__('Muri catalog deleted', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Muri catalog was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
}
