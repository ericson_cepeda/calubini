<?php
class MuriApplicationsController extends MuriAppController {

	var $name = 'MuriApplications';

	function application(){
		
	}

	function index() {
		$this->MuriApplication->recursive = 0;
		$this->set('muriApplications', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri application', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriApplication', $this->MuriApplication->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MuriApplication->create();
			if ($this->MuriApplication->save($this->data)) {
				$this->Session->setFlash(__('The muri application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri application could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri application', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriApplication->save($this->data)) {
				$this->Session->setFlash(__('The muri application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri application could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriApplication->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri application', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriApplication->delete($id)) {
			$this->Session->setFlash(__('Muri application deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri application was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('muriApplications', $this->MuriApplication->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri application', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriApplication', $this->MuriApplication->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriApplication->create();
			if ($this->MuriApplication->save($this->data)) {
				$this->Session->setFlash(__('The muri application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri application could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri application', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriApplication->save($this->data)) {
				$this->Session->setFlash(__('The muri application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri application could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriApplication->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri application', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriApplication->delete($id)) {
			$this->Session->setFlash(__('Muri application deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri application was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
