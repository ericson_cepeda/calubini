<?php
class MuriCategoryCaseStudiesController extends MuriAppController {

	var $name = 'MuriCategoryCaseStudies';

	function index() {
		$this->MuriCategoryCaseStudy->recursive = 0;
		$this->set('muriCategoryCaseStudies', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCategoryCaseStudy', $this->MuriCategoryCaseStudy->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MuriCategoryCaseStudy->create();
			if ($this->MuriCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The muri category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category case study could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The muri category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCategoryCaseStudy->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri category case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCategoryCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Muri category case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri category case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->MuriCategoryCaseStudy->recursive = 0;
		$this->set('muriCategoryCaseStudies', $this->MuriCategoryCaseStudy->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCategoryCaseStudy', $this->MuriCategoryCaseStudy->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriCategoryCaseStudy->create();
			if ($this->MuriCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The muri category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category case study could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The muri category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCategoryCaseStudy->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri category case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCategoryCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Muri category case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri category case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
