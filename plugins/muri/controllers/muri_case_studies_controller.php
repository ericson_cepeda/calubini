<?php
class MuriCaseStudiesController extends MuriAppController {

	var $name = 'MuriCaseStudies';

	function index($id = null){
		if(!empty($id)){
			$this->set('case_study', $this->MuriCaseStudy->find('first', array('conditions' => array('MuriCaseStudy.id' => $id))));
			$this->set('case_studies', $this->MuriCaseStudy->find('all', array('order' => array('MuriCaseStudy.order ASC'))));
			$this->set('id', $id);
		}
		else {
			$this->set('case_study', $this->MuriCaseStudy->find('first'));
			$this->set('case_studies', $this->MuriCaseStudy->find('all', array('order' => array('MuriCaseStudy.order ASC'))));
		}

	}

	function admin_index() {
		$this->set('muriCaseStudies', $this->MuriCaseStudy->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCaseStudy', $this->MuriCaseStudy->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriCaseStudy->create();
			if ($this->MuriCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The muri case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri case study could not be saved. Please, try again.', true));
			}
		}
        $this->set('categories', $this->MuriCaseStudy->MuriCategoryCaseStudy->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The muri case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCaseStudy->read(null, $id);
            $this->set('categories', $this->MuriCaseStudy->MuriCategoryCaseStudy->find('list', array('fields' => array('id', 'name'))));
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Muri case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
