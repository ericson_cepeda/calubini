<?php
class MuriArticlesController extends MuriAppController {

	var $name = 'MuriArticles';

	function index() {
		$this->MuriArticle->recursive = 0;
		$this->set('muriArticles', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri article', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriArticle', $this->MuriArticle->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MuriArticle->create();
			if ($this->MuriArticle->find('count') < 3)
				if ($this->MuriArticle->save($this->data)) {
					$this->Session->setFlash(__('The muri article has been saved', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The muri article could not be saved. Please, try again.', true));
				}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri article', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriArticle->save($this->data)) {
				$this->Session->setFlash(__('The muri article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri article could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriArticle->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriArticle->delete($id)) {
			$this->Session->setFlash(__('Muri article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri article was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('muriArticles', $this->MuriArticle->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri article', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriArticle', $this->MuriArticle->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriArticle->create();
			if ($this->MuriArticle->find('count') < 3)
				if ($this->MuriArticle->save($this->data)) {
					$this->Session->setFlash(__('The muri article has been saved', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The muri article could not be saved. Please, try again.', true));
				}
			else {
				$this->Session->setFlash(__('Maximum three articles are allowed.', true));
				$this->redirect(array('action' => 'index'));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri article', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriArticle->save($this->data)) {
				$this->Session->setFlash(__('The muri article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri article could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriArticle->read(null, $id);
        }
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriArticle->delete($id)) {
			$this->Session->setFlash(__('Muri article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri article was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
