<?php
class MuriCategoryCatalogsController extends MuriAppController {

	var $name = 'MuriCategoryCatalogs';

	function index() {
		$this->MuriCategoryCatalog->recursive = 0;
		$this->set('muriCategoryCatalogs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCategoryCatalog', $this->MuriCategoryCatalog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->MuriCategoryCatalog->create();
			if ($this->MuriCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCategoryCatalog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri category catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCategoryCatalog->delete($id)) {
			$this->Session->setFlash(__('Muri category catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri category catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('muriCategoryCatalogs', $this->MuriCategoryCatalog->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid muri category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('muriCategoryCatalog', $this->MuriCategoryCatalog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->MuriCategoryCatalog->create();
			if ($this->MuriCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid muri category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->MuriCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The muri category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The muri category catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->MuriCategoryCatalog->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for muri category catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->MuriCategoryCatalog->delete($id)) {
			$this->Session->setFlash(__('Muri category catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Muri category catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
