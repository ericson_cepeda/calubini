<?php
class MuriCategoryCatalog extends MuriAppModel {
	var $name = 'MuriCategoryCatalog';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	var $hasMany = array(
		'MuriCatalog' => array(
			'className' => 'MuriCatalog',
			'foreignKey' => 'muri_category_catalog_id'
		)
	);
}
