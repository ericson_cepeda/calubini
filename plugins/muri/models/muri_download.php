<?php
class MuriDownload extends MuriAppModel {
	var $name = 'MuriDownload';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'MuriCategoryDownload' => array(
			'className' => 'MuriCategoryDownload',
			'foreignKey' => 'muri_category_download_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    var $actsAs = array(
        'Upload.Upload' => array(
            'file' => array(
                'fields' => array(
                    'dir' => 'file_dir'
                )
            )
        )
    );
}
