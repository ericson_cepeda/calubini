<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Muri Category Case Study</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('MuriCategoryCaseStudy', array('action' => 'admin_add'));
                echo $form->input('MuriCategoryCaseStudy.name', array('label' => 'Name'));
                echo $form->input('MuriCategoryCaseStudy.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>