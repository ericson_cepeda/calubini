<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Newsletter Categorie</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_category_case_studies/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Newsletter', '/admin/muri/muri_category_case_studies/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriCategoryCaseStudies as $muriCategoryCaseStudie): ?>
    <tr>
        <td><?php echo $muriCategoryCaseStudie['MuriCategoryCaseStudy']['id']; ?></td>
        <td>
            <?php echo $muriCategoryCaseStudie['MuriCategoryCaseStudy']['name']; ?>
        </td>
        <td><?php echo $muriCategoryCaseStudie['MuriCategoryCaseStudy']['order']; ?></td>
        <td><?php echo $muriCategoryCaseStudie['MuriCategoryCaseStudy']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_category_case_studies','action'=>'edit',$muriCategoryCaseStudie['MuriCategoryCaseStudy']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_category_case_studies','action'=>'delete',$muriCategoryCaseStudie['MuriCategoryCaseStudy']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>