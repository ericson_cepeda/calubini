<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Category Downloads</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_category_downloads/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Catalog', '/admin/muri/muri_category_downloads/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriCategoryDownloads as $muriCategoryDownload): ?>
    <tr>
        <td><?php echo $muriCategoryDownload['MuriCategoryDownload']['id']; ?></td>
        <td>
            <?php echo $muriCategoryDownload['MuriCategoryDownload']['name']; ?>
        </td>
        <td><?php echo $muriCategoryDownload['MuriCategoryDownload']['order']; ?></td>
        <td><?php echo $muriCategoryDownload['MuriCategoryDownload']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_category_downloads','action'=>'edit',$muriCategoryDownload['MuriCategoryDownload']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_category_downloads','action'=>'delete',$muriCategoryDownload['MuriCategoryDownload']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>