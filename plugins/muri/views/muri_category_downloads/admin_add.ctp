<!-- File: /app/views/posts/add.ctp -->
<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Muri Catalog</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('MuriCategoryDownload', array('action' => 'admin_add'));
                echo $form->input('MuriCategoryDownload.name', array('label' => 'Name'));
                echo $form->input('MuriCategoryDownload.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>