<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Catalogs</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_catalogs/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Prodotto', '/admin/muri/muri_catalogs/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriCatalogs as $muriCatalog): ?>
    <tr>
        <td><?php echo $muriCatalog['MuriCatalog']['id']; ?></td>
        <td>
            <?php echo $muriCatalog['MuriCatalog']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/muri_catalog/photo/<?php echo $muriCatalog['MuriCatalog']['photo_dir']; ?>/thumb_<?php echo $muriCatalog['MuriCatalog']['photo'] ?>"></td>
        <td><?php echo $muriCatalog['MuriCatalog']['order']; ?></td>
        <td><?php echo $muriCatalog['MuriCatalog']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_catalogs','action'=>'edit',$muriCatalog['MuriCatalog']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_catalogs','action'=>'delete',$muriCatalog['MuriCatalog']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>