<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Muri Catalog</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('MuriCatalog', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('MuriCatalog.name', array('label' => 'Name'));
                echo $this->TinyMce->input('MuriCatalog.description');
                echo $this->Form->input('MuriCatalog.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('MuriCatalog.photo_dir', array('type' => 'hidden'));
                echo $this->Form->input('MuriCatalog.pdf', array('type' => 'file', 'label' => 'Pdf'));
                echo $this->Form->input('MuriCatalog.pdf_dir', array('type' => 'hidden'));
                echo $form->input('MuriCatalog.order', array('label' => 'Order'));
                echo $form->input('MuriCatalog.muri_category_catalog_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>