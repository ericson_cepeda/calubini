<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Applications</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_applications/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Applications', '/admin/muri/muri_applications/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriApplications as $muriApplication): ?>
    <tr>
        <td><?php echo $muriApplication['MuriApplication']['id']; ?></td>
        <td>
            <?php echo $muriApplication['MuriApplication']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/muri_application/img/<?php echo $muriApplication['MuriApplication']['img_dir']; ?>/thumb_<?php echo $muriApplication['MuriApplication']['img'] ?>"></td>
        <td><?php echo $muriApplication['MuriApplication']['order']; ?></td>
        <td><?php echo $muriApplication['MuriApplication']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_applications','action'=>'edit',$muriApplication['MuriApplication']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_applications','action'=>'delete',$muriApplication['MuriApplication']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>