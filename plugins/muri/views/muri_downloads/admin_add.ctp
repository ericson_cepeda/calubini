<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Download</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('MuriDownload', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('MuriDownload.name', array('label' => 'Name'));
                echo $this->TinyMce->input('MuriDownload.description');
                echo $this->Form->input('MuriDownload.file', array('type' => 'file', 'label' => 'Pdf'));
                echo $this->Form->input('MuriDownload.file_dir', array('type' => 'hidden'));
                echo $form->input('MuriDownload.order', array('label' => 'Order'));
                echo $form->input('MuriDownload.muri_category_download_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>