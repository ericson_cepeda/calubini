<!-- File: /app/views/posts/index.ctp -->

<h1>Downloads</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_downloads/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Download', '/admin/muri/muri_downloads/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriDownloads as $muriDownload): ?>
    <tr>
        <td><?php echo $muriDownload['MuriDownload']['id']; ?></td>
        <td>
            <?php echo $muriDownload['MuriDownload']['name']; ?>
        </td>
        <td><?= $html->link(__('Preview',true),$this->webroot."/files/muri_download/file/".$muriDownload['MuriDownload']['file_dir'].'/'.$muriDownload['MuriDownload']['file'],array()) ?></td>
        <td><?php echo $muriDownload['MuriDownload']['order']; ?></td>
        <td><?php echo $muriDownload['MuriDownload']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_downloads','action'=>'edit',$muriDownload['MuriDownload']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_downloads','action'=>'delete',$muriDownload['MuriDownload']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>