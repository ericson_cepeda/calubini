<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Category Catalog</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_category_catalogs/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Catalog', '/admin/muri/muri_category_catalogs/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriCategoryCatalogs as $muriCategoryCatalog): ?>
    <tr>
        <td><?php echo $muriCategoryCatalog['MuriCategoryCatalog']['id']; ?></td>
        <td>
            <?php echo $muriCategoryCatalog['MuriCategoryCatalog']['name']; ?>
        </td>
        <td><?php echo $muriCategoryCatalog['MuriCategoryCatalog']['order']; ?></td>
        <td><?php echo $muriCategoryCatalog['MuriCategoryCatalog']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_category_catalogs','action'=>'edit',$muriCategoryCatalog['MuriCategoryCatalog']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_category_catalogs','action'=>'delete',$muriCategoryCatalog['MuriCategoryCatalog']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>