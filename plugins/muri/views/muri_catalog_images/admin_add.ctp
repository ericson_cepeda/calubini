<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Muri Catalog Image</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('MuriCatalogImage', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('MuriCatalogImage.name', array('label' => 'Name'));
                echo $this->Form->input('MuriCatalogImage.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('MuriCatalogImage.photo_dir', array('type' => 'hidden'));
                echo $form->input('MuriCatalogImage.order', array('label' => 'Order'));
                echo $form->input('MuriCatalogImage.muri_catalog_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>