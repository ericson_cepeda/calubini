<!-- File: /app/views/posts/index.ctp --><h1>Muri Image Gallery</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_catalog_images/add', array('escape' => false)); ?>
<?php echo $html->link('Muri Newsletter Images', '/admin/muri/muri_catalog_images/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>
            Id
        </th>
        <th>
            Name
        </th>
        <th>
            Preview
        </th>
        <th>
            Order
        </th>
        <th>
            Created
        </th>
        <th>
            Action
        </th>
    </tr>
    <!-- Here is where we loop through our $posts array, printing out post info -->
    <?php foreach ($muriCatalogImages as $key => $muriCatalogImage): ?>
    <tr>
        <td>
            <?php echo $muriCatalogImage['MuriCatalogImage']['id']; ?>
        </td>
        <td>
            <?php echo $muriCatalogImage['MuriCatalogImage']['name']; ?>
        </td>
        <td>
            <img src="<?=SITE_URL?>files/muri_catalog_image/photo/<?= $muriCatalogImage['MuriCatalogImage']['id'] ?>/thumb_<?php echo $muriCatalogImage['MuriCatalogImage']['photo'] ?>">
        </td>
        <td>
            <?php echo $muriCatalogImage['MuriCatalogImage']['order']; ?>
        </td>
        <td>
            <?php echo $muriCatalogImage['MuriCatalogImage']['created']; ?>
        </td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_catalog_images','action'=>'edit',$muriCatalogImage['MuriCatalogImage']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_catalog_images','action'=>'delete',$muriCatalogImage['MuriCatalogImage']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
