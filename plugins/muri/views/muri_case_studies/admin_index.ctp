<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Newsletter</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_case_studies/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Newsletter', '/admin/muri/muri_case_studies/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriCaseStudies as $muriCaseStudie): ?>
    <tr>
        <td><?php echo $muriCaseStudie['MuriCaseStudy']['id']; ?></td>
        <td>
            <?php echo $muriCaseStudie['MuriCaseStudy']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/muri_case_study/photo/<?php echo $muriCaseStudie['MuriCaseStudy']['photo_dir']; ?>/thumb_<?php echo $muriCaseStudie['MuriCaseStudy']['photo'] ?>"></td>
        <td><?php echo $muriCaseStudie['MuriCaseStudy']['order']; ?></td>
        <td><?php echo $muriCaseStudie['MuriCaseStudy']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_case_studies','action'=>'edit', $muriCaseStudie['MuriCaseStudy']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_case_studies','action'=>'delete', $muriCaseStudie['MuriCaseStudy']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>