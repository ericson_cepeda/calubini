<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Muri Case Study</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('MuriCaseStudy', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('MuriCaseStudy.name', array('label' => 'Name'));
                echo $this->TinyMce->input('MuriCaseStudy.description');
                echo $this->Form->input('MuriCaseStudy.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('MuriCaseStudy.photo_dir', array('type' => 'hidden'));
                echo $form->input('MuriCaseStudy.album_link', array('label' => 'Album Link'));
                echo $form->input('MuriCaseStudy.youtube_link', array('label' => 'YouTube Link'));
                echo $form->input('MuriCaseStudy.order', array('label' => 'Order'));
                echo $form->input('MuriCaseStudy.muri_category_case_study_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>