<!-- File: /app/views/posts/index.ctp -->

<h1>Muri Articles</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/muri/muri_articles/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Articolo', '/admin/muri/muri_articles/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($muriArticles as $muriArticle): ?>
    <tr>
        <td><?php echo $muriArticle['MuriArticle']['id']; ?></td>
        <td>
            <?php echo $muriArticle['MuriArticle']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/muri_article/photo/<?php echo $muriArticle['MuriArticle']['photo_dir']; ?>/thumb_<?php echo $muriArticle['MuriArticle']['photo'] ?>"></td>
        <td><?php echo $muriArticle['MuriArticle']['order']; ?></td>
        <td><?php echo $muriArticle['MuriArticle']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'muri_articles','action'=>'edit',$muriArticle['MuriArticle']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'muri_articles','action'=>'delete',$muriArticle['MuriArticle']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>