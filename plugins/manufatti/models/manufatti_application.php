<?php
class ManufattiApplication extends ManufattiAppModel {
	var $name = 'ManufattiApplication';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
	);
    var $actsAs = array(
        'Upload.Upload' => array(
            'img' => array(
                'fields' => array(
                    'dir' => 'img_dir'
                ),
                'thumbsizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '601x399',
                    'thumb' => '116x92'
                ),
                'thumbnailMethod' => 'php'
            )
        )
    );
}
