<?php
class ManufattiCatalogImage extends ManufattiAppModel {
    var $name = 'ManufattiCatalogImage';
    var $displayField = 'name';
    var $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    var $belongsTo = array(
        'ManufattiCatalog' => array(
            'className' => 'ManufattiCatalog',
            'foreignKey' => 'manufatti_catalog_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $actsAs = array(
        'Upload.Upload' => array(
            'photo' => array(
                'fields' => array(
                    'dir' => 'photo_dir'
                ),
                'thumbsizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '640x480',
                    'thumb' => '252x244'
                ),
                'thumbnailMethod' => 'php'
            )
        )
    );
}
