<?php
class ManufattiCategoryCatalog extends ManufattiAppModel {
	var $name = 'ManufattiCategoryCatalog';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	var $hasMany = array(
		'ManufattiCatalog' => array(
			'className' => 'ManufattiCatalog',
			'foreignKey' => 'manufatti_category_catalog_id'
		)
	);
}
