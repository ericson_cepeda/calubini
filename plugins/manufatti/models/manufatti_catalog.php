<?php
class ManufattiCatalog extends ManufattiAppModel {
	var $name = 'ManufattiCatalog';
	var $displayField = 'name';
	var $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	var $belongsTo = array(
		'ManufattiCategoryCatalog' => array(
			'className' => 'ManufattiCategoryCatalog',
			'foreignKey' => 'manufatti_category_catalog_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    var $hasMany = array(
        'ManufattiCatalogImage' => array(
            'className' => 'ManufattiCatalogImage',
            'foreign_key' => 'muri_catalog'
        )
    );
    var $actsAs = array(
        'Upload.Upload' => array(
            'photo' => array(
                'fields' => array(
                    'dir' => 'photo_dir'
                ),
                'thumbsizes' => array(
                    'xvga' => '1024x768',
                    'vga' => '640x480',
                    'thumb' => '252x244'
                ),
                'thumbnailMethod' => 'php'
            ),
            'pdf' => array(
                'fields' => array(
                    'dir' => 'pdf_dir'
                )
            )
        )
    );
}
