<?php
class ManufattiCategoryDownloadsController extends ManufattiAppController {

	var $name = 'ManufattiCategoryDownloads';

	function index() {
		$this->ManufattiCategoryDownload->recursive = 0;
		$this->set('manufattiCategoryDownloads', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti category download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCategoryDownload', $this->ManufattiCategoryDownload->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiCategoryDownload->create();
			if ($this->ManufattiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category download could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti category download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCategoryDownload->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti category download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCategoryDownload->delete($id)) {
			$this->Session->setFlash(__('Manufatti category download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti category download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->ManufattiCategoryDownload->recursive = 0;
		$this->set('manufattiCategoryDownloads', $this->ManufattiCategoryDownload->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti category download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCategoryDownload', $this->ManufattiCategoryDownload->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiCategoryDownload->create();
			if ($this->ManufattiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category download could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti category download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCategoryDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCategoryDownload->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti category download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCategoryDownload->delete($id)) {
			$this->Session->setFlash(__('Manufatti category download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti category download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
