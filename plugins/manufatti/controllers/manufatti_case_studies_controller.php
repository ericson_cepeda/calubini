<?php
class ManufattiCaseStudiesController extends ManufattiAppController {

	var $name = 'ManufattiCaseStudies';

	function index() {
		$this->ManufattiCaseStudy->recursive = 0;
		$this->set('manufattiCaseStudies', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCaseStudy', $this->ManufattiCaseStudy->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiCaseStudy->create();
			if ($this->ManufattiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti case study could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCaseStudy->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Manufatti case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('manufattiCaseStudies', $this->ManufattiCaseStudy->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCaseStudy', $this->ManufattiCaseStudy->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiCaseStudy->create();
			if ($this->ManufattiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti case study could not be saved. Please, try again.', true));
			}
		}
        $this->set('categories', $this->ManufattiCaseStudy->ManufattiCategoryCaseStudy->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCaseStudy->read(null, $id);
            $this->set('categories', $this->ManufattiCaseStudy->ManufattiCategoryCaseStudy->find('list', array('fields' => array('id', 'name'))));
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Manufatti case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
