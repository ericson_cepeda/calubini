<?php
class ManufattiDownloadsController extends ManufattiAppController {

	var $name = 'ManufattiDownloads';

	function index() {
		$this->ManufattiDownload->recursive = 0;
		$this->set('manufattiDownloads', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiDownload', $this->ManufattiDownload->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiDownload->create();
			if ($this->ManufattiDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti download could not be saved. Please, try again.', true));
			}
		}
		$manufattiCategoryDownloads = $this->ManufattiDownload->ManufattiCategoryDownload->find('list');
		$this->set(compact('manufattiCategoryDownloads'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiDownload->read(null, $id);
		}
		$manufattiCategoryDownloads = $this->ManufattiDownload->ManufattiCategoryDownload->find('list');
		$this->set(compact('manufattiCategoryDownloads'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiDownload->delete($id)) {
			$this->Session->setFlash(__('Manufatti download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('manufattiDownloads', $this->ManufattiDownload->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti download', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiDownload', $this->ManufattiDownload->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiDownload->create();
			if ($this->ManufattiDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti download could not be saved. Please, try again.', true));
			}
		}
    $this->set('categories', $this->ManufattiDownload->ManufattiCategoryDownload->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti download', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiDownload->save($this->data)) {
				$this->Session->setFlash(__('The manufatti download has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti download could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiDownload->read(null, $id);
		}
    $this->set('categories', $this->ManufattiDownload->ManufattiCategoryDownload->find('list', array('fields' => array('id', 'name'))));
  }

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti download', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiDownload->delete($id)) {
			$this->Session->setFlash(__('Manufatti download deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti download was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
