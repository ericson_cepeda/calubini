<?php
class ManufattiApplicationsController extends ManufattiAppController {

	var $name = 'ManufattiApplications';

	function index() {
		$this->ManufattiApplication->recursive = 0;
		$this->set('manufattiApplications', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti application', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiApplication', $this->ManufattiApplication->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiApplication->create();
			if ($this->ManufattiApplication->save($this->data)) {
				$this->Session->setFlash(__('The manufatti application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti application could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti application', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiApplication->save($this->data)) {
				$this->Session->setFlash(__('The manufatti application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti application could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiApplication->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti application', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiApplication->delete($id)) {
			$this->Session->setFlash(__('Manufatti application deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti application was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('manufattiApplications', $this->ManufattiApplication->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti application', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiApplication', $this->ManufattiApplication->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiApplication->create();
			if ($this->ManufattiApplication->save($this->data)) {
				$this->Session->setFlash(__('The manufatti application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti application could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti application', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiApplication->save($this->data)) {
				$this->Session->setFlash(__('The manufatti application has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti application could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiApplication->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti application', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiApplication->delete($id)) {
			$this->Session->setFlash(__('Manufatti application deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti application was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
