<?php
class ManufattiCategoryCaseStudiesController extends ManufattiAppController {

	var $name = 'ManufattiCategoryCaseStudies';

	function index() {
		$this->ManufattiCategoryCaseStudy->recursive = 0;
		$this->set('manufattiCategoryCaseStudies', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCategoryCaseStudy', $this->ManufattiCategoryCaseStudy->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiCategoryCaseStudy->create();
			if ($this->ManufattiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category case study could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCategoryCaseStudy->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti category case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCategoryCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Manufatti category case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti category case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->ManufattiCategoryCaseStudy->recursive = 0;
		$this->set('manufattiCategoryCaseStudies', $this->ManufattiCategoryCaseStudy->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCategoryCaseStudy', $this->ManufattiCategoryCaseStudy->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiCategoryCaseStudy->create();
			if ($this->ManufattiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category case study could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti category case study', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCategoryCaseStudy->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category case study has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category case study could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCategoryCaseStudy->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti category case study', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCategoryCaseStudy->delete($id)) {
			$this->Session->setFlash(__('Manufatti category case study deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti category case study was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
