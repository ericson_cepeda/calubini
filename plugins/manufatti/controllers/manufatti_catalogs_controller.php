<?php
class ManufattiCatalogsController extends ManufattiAppController {

	var $name = 'ManufattiCatalogs';

	function index() {
		$this->ManufattiCatalog->recursive = 0;
		$this->set('manufattiCatalogs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCatalog', $this->ManufattiCatalog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiCatalog->create();
			if ($this->ManufattiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCatalog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCatalog->delete($id)) {
			$this->Session->setFlash(__('Manufatti catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('manufattiCatalogs', $this->ManufattiCatalog->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCatalog', $this->ManufattiCatalog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiCatalog->create();
			if ($this->ManufattiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti catalog could not be saved. Please, try again.', true));
			}
		}
        $this->set('categories', $this->ManufattiCatalog->ManufattiCategoryCatalog->find('list', array('fields' => array('id', 'name'))));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCatalog->read(null, $id);
            $this->set('categories', $this->ManufattiCatalog->ManufattiCategoryCatalog->find('list', array('fields' => array('id', 'name'))));
        }
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCatalog->delete($id)) {
			$this->Session->setFlash(__('Manufatti catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
