<?php
class ManufattiCatalogImagesController extends ManufattiAppController {

    var $name = 'ManufattiCatalogImages';

    function admin_index() {
        $this->set('manufattiCatalogImages', $this->ManufattiCatalogImage->find('all'));
    }

    function admin_view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid manufatti catalog', true));
            $this->redirect(array('action' => 'index'));
        }
        $this->set('manufattiCatalogImages', $this->ManufattiCatalogImage->read(null, $id));
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->ManufattiCatalogImage->create();
            if ($this->ManufattiCatalogImage->save($this->data)) {
                $this->Session->setFlash(__('The manufatti catalog has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The manufatti catalog could not be saved. Please, try again.', true));
            }
        }
        $this->set('categories', $this->ManufattiCatalogImage->ManufattiCatalog->find('list', array('fields' => array('id', 'name'))));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid manufatti catalog', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->ManufattiCatalogImage->save($this->data)) {
                $this->Session->setFlash(__('The manufatti catalog has been saved', true));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The manufatti catalog could not be saved. Please, try again.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->ManufattiCatalogImage->read(null, $id);
            $this->set('categories', $this->ManufattiCatalogImage->ManufattiCatalog->find('list', array('fields' => array('id', 'name'))));
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for manufatti catalog', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->ManufattiCatalogImage->delete($id)) {
            $this->Session->setFlash(__('Manufatti catalog deleted', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->Session->setFlash(__('Manufatti catalog was not deleted', true));
        $this->redirect(array('action' => 'index'));
    }
}
