<?php
class ManufattiCategoryCatalogsController extends ManufattiAppController {

	var $name = 'ManufattiCategoryCatalogs';

	function index() {
		$this->ManufattiCategoryCatalog->recursive = 0;
		$this->set('manufattiCategoryCatalogs', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCategoryCatalog', $this->ManufattiCategoryCatalog->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiCategoryCatalog->create();
			if ($this->ManufattiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCategoryCatalog->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti category catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCategoryCatalog->delete($id)) {
			$this->Session->setFlash(__('Manufatti category catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti category catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('manufattiCategoryCatalogs', $this->ManufattiCategoryCatalog->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiCategoryCatalog', $this->ManufattiCategoryCatalog->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiCategoryCatalog->create();
			if ($this->ManufattiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category catalog could not be saved. Please, try again.', true));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti category catalog', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiCategoryCatalog->save($this->data)) {
				$this->Session->setFlash(__('The manufatti category catalog has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti category catalog could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiCategoryCatalog->read(null, $id);
		}
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti category catalog', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiCategoryCatalog->delete($id)) {
			$this->Session->setFlash(__('Manufatti category catalog deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti category catalog was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
