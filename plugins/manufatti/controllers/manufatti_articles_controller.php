<?php
class ManufattiArticlesController extends ManufattiAppController {

	var $name = 'ManufattiArticles';

	function index() {
		$this->ManufattiArticle->recursive = 0;
		$this->set('manufattiArticles', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti article', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiArticle', $this->ManufattiArticle->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			$this->ManufattiArticle->create();
			if ($this->ManufattiArticle->find('count') < 3)
				if ($this->ManufattiArticle->save($this->data)) {
					$this->Session->setFlash(__('The manufatti article has been saved', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The manufatti article could not be saved. Please, try again.', true));
				}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti article', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiArticle->save($this->data)) {
				$this->Session->setFlash(__('The manufatti article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti article could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiArticle->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiArticle->delete($id)) {
			$this->Session->setFlash(__('Manufatti article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti article was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
	function admin_index() {
		$this->set('manufattiArticles', $this->ManufattiArticle->find('all'));
	}

	function admin_view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid manufatti article', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('manufattiArticle', $this->ManufattiArticle->read(null, $id));
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->ManufattiArticle->create();
			if ($this->ManufattiArticle->find('count') < 3)
				if ($this->ManufattiArticle->save($this->data)) {
					$this->Session->setFlash(__('The manufatti article has been saved', true));
					$this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The manufatti article could not be saved. Please, try again.', true));
				}
			else{
				$this->Session->setFlash(__('Maximum three articles are allowed.', true));
				$this->redirect(array('action' => 'index'));
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid manufatti article', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->ManufattiArticle->save($this->data)) {
				$this->Session->setFlash(__('The manufatti article has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The manufatti article could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->ManufattiArticle->read(null, $id);
            $this->set('categories', $this->ManufattiArticle->ManufattiCategoryArticle->find('list', array('fields' => array('id', 'name'))));
        }
	}

	function admin_delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for manufatti article', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->ManufattiArticle->delete($id)) {
			$this->Session->setFlash(__('Manufatti article deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Manufatti article was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}
}
