<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Manufatti Article</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiArticle', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('ManufattiArticle.name', array('label' => 'Name'));
                echo $this->TinyMce->input('ManufattiArticle.description');
                echo $this->Form->input('ManufattiArticle.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('ManufattiArticle.photo_dir', array('type' => 'hidden'));
                echo $form->input('ManufattiArticle.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>