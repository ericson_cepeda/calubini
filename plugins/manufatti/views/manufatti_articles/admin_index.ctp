<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Articles</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_articles/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Articolo', '/admin/manufatti/manufatti_articles/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiArticles as $manufattiArticle): ?>
    <tr>
        <td><?php echo $manufattiArticle['ManufattiArticle']['id']; ?></td>
        <td>
            <?php echo $manufattiArticle['ManufattiArticle']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/manufatti_article/photo/<?php echo $manufattiArticle['ManufattiArticle']['photo_dir']; ?>/thumb_<?php echo $manufattiArticle['ManufattiArticle']['photo'] ?>"></td>
        <td><?php echo $manufattiArticle['ManufattiArticle']['order']; ?></td>
        <td><?php echo $manufattiArticle['ManufattiArticle']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_articles','action'=>'edit',$manufattiArticle['ManufattiArticle']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_articles','action'=>'delete',$manufattiArticle['ManufattiArticle']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>