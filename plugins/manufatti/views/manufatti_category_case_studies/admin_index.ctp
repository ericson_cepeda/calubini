<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Newsletter Categorie</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_category_case_studies/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Newsletter', '/admin/manufatti/manufatti_category_case_studies/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiCategoryCaseStudies as $manufattiCategoryCaseStudie): ?>
    <tr>
        <td><?php echo $manufattiCategoryCaseStudie['ManufattiCategoryCaseStudy']['id']; ?></td>
        <td>
            <?php echo $manufattiCategoryCaseStudie['ManufattiCategoryCaseStudy']['name']; ?>
        </td>
        <td><?php echo $manufattiCategoryCaseStudie['ManufattiCategoryCaseStudy']['order']; ?></td>
        <td><?php echo $manufattiCategoryCaseStudie['ManufattiCategoryCaseStudy']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_category_case_studies','action'=>'edit',$manufattiCategoryCaseStudie['ManufattiCategoryCaseStudy']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_category_case_studies','action'=>'delete',$manufattiCategoryCaseStudie['ManufattiCategoryCaseStudy']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>