<!-- File: /app/views/posts/add.ctp -->
<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Manufatti Catalog</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiCategoryCatalog', array('action' => 'admin_edit'));
                echo $form->input('ManufattiCategoryCatalog.id', array('type' => 'hidden'));
                echo $form->input('ManufattiCategoryCatalog.name', array('label' => 'Name'));
                echo $form->input('ManufattiCategoryCatalog.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>