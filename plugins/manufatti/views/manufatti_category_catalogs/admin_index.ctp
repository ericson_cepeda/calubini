<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Category Catalog</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_category_catalogs/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Catalog', '/admin/manufatti/manufatti_category_catalogs/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiCategoryCatalogs as $manufattiCategoryCatalog): ?>
    <tr>
        <td><?php echo $manufattiCategoryCatalog['ManufattiCategoryCatalog']['id']; ?></td>
        <td>
            <?php echo $manufattiCategoryCatalog['ManufattiCategoryCatalog']['name']; ?>
        </td>
        <td><?php echo $manufattiCategoryCatalog['ManufattiCategoryCatalog']['order']; ?></td>
        <td><?php echo $manufattiCategoryCatalog['ManufattiCategoryCatalog']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_category_catalogs','action'=>'edit',$manufattiCategoryCatalog['ManufattiCategoryCatalog']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_category_catalogs','action'=>'delete',$manufattiCategoryCatalog['ManufattiCategoryCatalog']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>