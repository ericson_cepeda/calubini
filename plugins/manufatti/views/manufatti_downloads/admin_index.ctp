<!-- File: /app/views/posts/index.ctp -->

<h1>Downloads</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_downloads/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Download', '/admin/manufatti/manufatti_downloads/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiDownloads as $manufattiDownload): ?>
    <tr>
        <td><?php echo $manufattiDownload['ManufattiDownload']['id']; ?></td>
        <td>
            <?php echo $manufattiDownload['ManufattiDownload']['name']; ?>
        </td>
		<td><?= $html->link(__('Preview',true),$this->webroot."/files/manufatti_download/file/".$manufattiDownload['ManufattiDownload']['file_dir'].'/'.$manufattiDownload['ManufattiDownload']['file'],array()) ?></td>
        <td><?php echo $manufattiDownload['ManufattiDownload']['order']; ?></td>
        <td><?php echo $manufattiDownload['ManufattiDownload']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_downloads','action'=>'edit',$manufattiDownload['ManufattiDownload']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_downloads','action'=>'delete',$manufattiDownload['ManufattiDownload']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>