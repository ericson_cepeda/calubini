<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Download</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiDownload', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('ManufattiDownload.name', array('label' => 'Name'));
                echo $this->TinyMce->input('ManufattiDownload.description');
                echo $this->Form->input('ManufattiDownload.file', array('type' => 'file', 'label' => 'Pdf'));
                echo $this->Form->input('ManufattiDownload.file_dir', array('type' => 'hidden'));
                echo $form->input('ManufattiDownload.order', array('label' => 'Order'));
                echo $form->input('ManufattiDownload.manufatti_category_download_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>