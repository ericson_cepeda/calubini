<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Manufatti Case Study</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiCaseStudy', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('ManufattiCaseStudy.id', array('type' => 'hidden'));
                echo $form->input('ManufattiCaseStudy.name', array('label' => 'Name'));
                echo $form->input('ManufattiCaseStudy.description', array('label' => 'Name', 'type' => 'textarea'));
                echo $this->Form->input('ManufattiCaseStudy.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('ManufattiCaseStudy.photo_dir', array('type' => 'hidden'));
                echo $form->input('ManufattiCaseStudy.album_link', array('label' => 'Album Link'));
                echo $form->input('ManufattiCaseStudy.youtube_link', array('label' => 'YouTube Link'));
                echo $form->input('ManufattiCaseStudy.order', array('label' => 'Order'));
                echo $form->input('ManufattiCaseStudy.muri_category_case_study_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>