<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Newsletter</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_case_studies/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Newsletter', '/admin/manufatti/manufatti_case_studies/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiCaseStudies as $manufattiCaseStudie): ?>
    <tr>
        <td><?php echo $manufattiCaseStudie['ManufattiCaseStudy']['id']; ?></td>
        <td>
            <?php echo $manufattiCaseStudie['ManufattiCaseStudy']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/manufatti_case_study/photo/<?php echo $manufattiCaseStudie['ManufattiCaseStudy']['photo_dir']; ?>/thumb_<?php echo $manufattiCaseStudie['ManufattiCaseStudy']['photo'] ?>"</td>
        <td><?php echo $manufattiCaseStudie['ManufattiCaseStudy']['order']; ?></td>
        <td><?php echo $manufattiCaseStudie['ManufattiCaseStudy']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_case_studies','action'=>'edit', $manufattiCaseStudie['ManufattiCaseStudy']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_case_studies','action'=>'delete', $manufattiCaseStudie['ManufattiCaseStudy']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>