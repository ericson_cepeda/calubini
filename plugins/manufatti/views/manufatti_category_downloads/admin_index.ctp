<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Category Downloads</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_category_downloads/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Categoria Catalog', '/admin/manufatti/manufatti_category_downloads/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiCategoryDownloads as $manufattiCategoryDownload): ?>
    <tr>
        <td><?php echo $manufattiCategoryDownload['ManufattiCategoryDownload']['id']; ?></td>
        <td>
            <?php echo $manufattiCategoryDownload['ManufattiCategoryDownload']['name']; ?>
        </td>
        <td><?php echo $manufattiCategoryDownload['ManufattiCategoryDownload']['order']; ?></td>
        <td><?php echo $manufattiCategoryDownload['ManufattiCategoryDownload']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_category_downloads','action'=>'edit',$manufattiCategoryDownload['ManufattiCategoryDownload']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_category_downloads','action'=>'delete',$manufattiCategoryDownload['ManufattiCategoryDownload']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>