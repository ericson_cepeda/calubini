<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Applications</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_applications/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Applications', '/admin/manufatti/manufatti_applications/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiApplications as $manufattiApplication): ?>
    <tr>
        <td><?php echo $manufattiApplication['ManufattiApplication']['id']; ?></td>
        <td>
            <?php echo $manufattiApplication['ManufattiApplication']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/manufatti_application/img/<?php echo $manufattiApplication['ManufattiApplication']['img_dir']; ?>/thumb_<?php echo $manufattiApplication['ManufattiApplication']['img'] ?>"></td>
        <td><?php echo $manufattiApplication['ManufattiApplication']['order']; ?></td>
        <td><?php echo $manufattiApplication['ManufattiApplication']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_applications','action'=>'edit',$manufattiApplication['ManufattiApplication']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_applications','action'=>'delete',$manufattiApplication['ManufattiApplication']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>