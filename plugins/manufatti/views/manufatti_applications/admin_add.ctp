<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Add Application</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiApplication', array('type' => 'file', 'action' => 'admin_add'));
                echo $form->input('ManufattiApplication.name', array('label' => 'Name'));
                echo $this->Form->input('ManufattiApplication.img', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('ManufattiApplication.img_dir', array('type' => 'hidden'));
                echo $form->input('ManufattiApplication.order', array('label' => 'Order'));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>