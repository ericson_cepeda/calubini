<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Image Gallery</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_catalog_images/add', array('escape' => false)); ?><?php echo $html->link('Manufatti Newsletter Images', '/admin/manufatti/manufatti_catalog_images/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiCatalogImages as $manufattiCatalogImage): ?>
    <tr>
        <td><?php echo $manufattiCatalogImage['ManufattiCatalogImage']['id']; ?></td>
        <td>
            <?php echo $manufattiCatalogImage['ManufattiCatalogImage']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/manufatti_catalog_image/photo/<?php echo $manufattiCatalogImage['ManufattiCatalogImage']['id']; ?>/thumb_<?php echo $manufattiCatalogImage['ManufattiCatalogImage']['photo'] ?>"></td>
        <td><?php echo $manufattiCatalogImage['ManufattiCatalogImage']['order']; ?></td>
        <td><?php echo $manufattiCatalogImage['ManufattiCatalogImage']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_catalog_images','action'=>'edit',$manufattiCatalogImage['ManufattiCatalogImage']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_catalog_images','action'=>'delete',$manufattiCatalogImage['ManufattiCatalogImage']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>