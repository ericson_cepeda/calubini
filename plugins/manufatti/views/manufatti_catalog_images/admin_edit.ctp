<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Manufatti Catalog Image</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiCatalogImage', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('ManufattiCatalogImage.id', array('type' => 'hidden'));
                echo $form->input('ManufattiCatalogImage.name', array('label' => 'Name'));
                echo $this->Form->input('ManufattiCatalogImage.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('ManufattiCatalogImage.photo_dir', array('type' => 'hidden'));
                echo $form->input('ManufattiCatalogImage.order', array('label' => 'Order'));
                echo $form->input('ManufattiCatalogImage.manufatti_catalog_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>