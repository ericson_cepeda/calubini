<div class="row">
    <div id="survey_wizard" class="span10 offset1">
        <div class="widget">
            <div class="widget-header">
                <h3>Edit Manufatti Catalog</h3>
            </div>
            <div class="widget-content">
                <?php
                echo $form->create('ManufattiCatalog', array('type' => 'file', 'action' => 'admin_edit'));
                echo $form->input('ManufattiCatalog.id', array('type' => 'hidden'));
                echo $form->input('ManufattiCatalog.name', array('label' => 'Name'));
                echo $this->TinyMce->input('ManufattiCatalog.description');
                echo $this->Form->input('ManufattiCatalog.photo', array('type' => 'file', 'label' => 'Immagine'));
                echo $this->Form->input('ManufattiCatalog.photo_dir', array('type' => 'hidden'));
                echo $this->Form->input('ManufattiCatalog.pdf', array('type' => 'file', 'label' => 'Pdf'));
                echo $this->Form->input('ManufattiCatalog.pdf_dir', array('type' => 'hidden'));
                echo $form->input('ManufattiCatalog.order', array('label' => 'Order'));
                echo $form->input('ManufattiCatalog.manufatti_category_catalog_id', array('options' => $categories));
                echo $form->submit('Save', array('class' => "btn"));
                echo $form->end();
                ?>
            </div>
        </div>
    </div>
</div>