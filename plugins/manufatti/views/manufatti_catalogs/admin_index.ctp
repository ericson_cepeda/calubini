<!-- File: /app/views/posts/index.ctp -->

<h1>Manufatti Catalogs</h1>
<?php echo $html->link($html->image('admin/add.png'), '/admin/manufatti/manufatti_catalogs/add', array('escape' => false)); ?><?php echo $html->link('Aggiungi Prodotto', '/admin/manufatti/manufatti_catalogs/add'); ?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Preview</th>
        <th>Order</th>
        <th>Created</th>
        <th>Action</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($manufattiCatalogs as $manufattiCatalog): ?>
    <tr>
        <td><?php echo $manufattiCatalog['ManufattiCatalog']['id']; ?></td>
        <td>
            <?php echo $manufattiCatalog['ManufattiCatalog']['name']; ?>
        </td>
        <td><img src="<?=SITE_URL?>files/manufatti_catalog/photo/<?php echo $manufattiCatalog['ManufattiCatalog']['photo_dir']; ?>/thumb_<?php echo $manufattiCatalog['ManufattiCatalog']['photo'] ?>"</td>
        <td><?php echo $manufattiCatalog['ManufattiCatalog']['order']; ?></td>
        <td><?php echo $manufattiCatalog['ManufattiCatalog']['created']; ?></td>
        <td>
            <?php echo $this->Html->image('admin/edit.jpg',array('url'=>array('controller'=>'manufatti_catalogs','action'=>'edit',$manufattiCatalog['ManufattiCatalog']['id'], 'admin' => true)))." | ".$this->Html->image('admin/cancel.jpg',array('url'=>array('controller'=>'manufatti_catalogs','action'=>'delete',$manufattiCatalog['ManufattiCatalog']['id'], 'admin' => true))); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>